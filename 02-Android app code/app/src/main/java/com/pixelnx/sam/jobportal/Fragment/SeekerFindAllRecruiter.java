package com.pixelnx.sam.jobportal.Fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Activity.SeekerDashboardActivity;
import com.pixelnx.sam.jobportal.Adapter.AdapterDashboard;
import com.pixelnx.sam.jobportal.Adapter.AdapterShowAllRecruiter;
import com.pixelnx.sam.jobportal.DTOCI.AllJobs;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelSeekerLogin;
import com.pixelnx.sam.jobportal.DTOCI.ShowAllRecruiter;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class SeekerFindAllRecruiter extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private LinearLayout searchLayout;
    private RecyclerView recyclerview;
    private SwipeRefreshLayout swipeRefreshLayout;
    private InputMethodManager inputManager;
    private View view;
    private SharedPrefrence prefrence;
    private CustomEdittext etSearch;
    private ImageView ivSearch;
    private ImageView img;
    private AnimationDrawable anim;
    private ModelLanguageDTO.Data data;
    private ModelSeekerLogin userSeekerDTO;
    private ArrayList<ShowAllRecruiter.Data> recruiterList;
    private AdapterShowAllRecruiter adapterShowAllRecruiter;
    private LinearLayoutManager linearLayoutManagerVertical;
    private SeekerDashboardActivity seekerDashboardActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_seeker_find_all_recruiter, container, false);
        inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        prefrence = SharedPrefrence.getInstance(getActivity());
        data = prefrence.getLanguage(AppConstans.LANGUAGE).getData();
        userSeekerDTO = prefrence.getUserDTO(AppConstans.SEEKERDTO);
        init(view);
        // Inflate the layout for this fragment
        return view;
    }

    private void init(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        searchLayout = (LinearLayout) view.findViewById(R.id.searchLayout);
        etSearch = (CustomEdittext) view.findViewById(R.id.etSearch);
        ivSearch = (ImageView) view.findViewById(R.id.ivSearch);
        img = (ImageView) view.findViewById(R.id.img);

            seekerDashboardActivity.tvJobs.setVisibility(View.VISIBLE);
        seekerDashboardActivity.tvJobs.setText(data.getMy_jobs1());
        seekerDashboardActivity.search_icon.setVisibility(View.VISIBLE);
        seekerDashboardActivity.filter_icon.setVisibility(View.GONE);

        seekerDashboardActivity.search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchLayout.getVisibility() == View.VISIBLE) {
                    etSearch.setText("");
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    seekerDashboardActivity.search_icon.setImageResource(R.drawable.search);
                    searchLayout.setVisibility(View.GONE);

                } else {
                    seekerDashboardActivity.search_icon.setImageResource(R.drawable.close);
                    etSearch.requestFocus();
                    inputManager.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
                    searchLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        // Capture Text in EditText
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
                String text = etSearch.getText().toString().toLowerCase();
                if (recruiterList.size() != 0)
                    adapterShowAllRecruiter.filter(text);
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        getAllRecruiter();
                                        Log.e("Runnable", "FIRST");
                                        if (NetworkManager.isConnectToInternet(getActivity())) {
                                            swipeRefreshLayout.setRefreshing(false);
                                        } else {
                                            ProjectUtils.showToast(getActivity(), getResources().getString(R.string.internet_connection));
                                        }
                                    }
                                }
        );
    }




    @Override
    public void onRefresh() {
        getAllRecruiter();
    }

    private void getAllRecruiter() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppConstans.BASE_URL + AppConstans.ALL_RECRUITER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            ShowAllRecruiter recList = new ShowAllRecruiter();
                            recList = new Gson().fromJson(response, ShowAllRecruiter.class);
                            if (recList.getStaus().equals("true")) {
                                recruiterList = new ArrayList<>();
                                swipeRefreshLayout.setRefreshing(false);
                                recruiterList = recList.getData();
                                linearLayoutManagerVertical = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                recyclerview.setLayoutManager(linearLayoutManagerVertical);

                                if (recruiterList.size() != 0) {
                                                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //Do something after 1s

                                            adapterShowAllRecruiter = new AdapterShowAllRecruiter(recruiterList, getActivity(),userSeekerDTO.getBase_url());
                                            recyclerview.setAdapter(adapterShowAllRecruiter);

                                        }
                                    }, 500);

                                    recyclerview.setVisibility(View.VISIBLE);
                                    /*llIcon.setVisibility(View.GONE);*/
                                }

                            } else {
                                swipeRefreshLayout.setRefreshing(false);
                                ProjectUtils.showToast(getActivity(), recList.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProjectUtils.pauseProgressDialog();
                        swipeRefreshLayout.setRefreshing(false);

                        Log.d("ALL JOB RESPONSE", "Login Error:" + error);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        seekerDashboardActivity = (SeekerDashboardActivity) activity;
    }


}

