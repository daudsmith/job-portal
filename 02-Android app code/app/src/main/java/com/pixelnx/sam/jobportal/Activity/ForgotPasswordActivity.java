package com.pixelnx.sam.jobportal.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.DTO.CommonDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.RegisterActivity;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.CustomTextHeader;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup radioGroup;
    private RadioButton RBSeeker, RBRecruiter;
    private CustomEdittext etEmail, etPassword;
    private CustomTextHeader tvEmail;
    private CustomButton btnSubmit;
    private AdView mAdView;
    private CustomTextview tvCreatnewaccount, tvForgotpass;
    private Context mContext;
    private SharedPrefrence prefrence;
    private ModelLanguageDTO.Data modelLanguageDTO;
        private AdView adView;
        private   View adMobView;
        private CustomTextviewBold tvyouremail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mContext = ForgotPasswordActivity.this;
        prefrence = SharedPrefrence.getInstance(mContext);
        modelLanguageDTO=prefrence.getLanguage(AppConstans.LANGUAGE).getData();
        init();
    }

    private void init() {

        etEmail = (CustomEdittext) findViewById(R.id.etEmail);


        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        RBSeeker = (RadioButton) findViewById(R.id.RBSeeker);
        RBRecruiter = (RadioButton) findViewById(R.id.RBRecruiter);
        btnSubmit = (CustomButton) findViewById(R.id.btnSubmit);
        tvEmail = (CustomTextHeader) findViewById(R.id.tvEmail);
        tvyouremail = (CustomTextviewBold) findViewById(R.id.tvyouremail);
       adMobView = findViewById(R.id.adMobView);
        changelanguage();
        btnSubmit.setOnClickListener(this);
             //   ProjectUtils.showAdd(ForgotPasswordActivity.this, mAdView, "ca-app-pub-3940256099942544/6300978111", adMobView);
    }

    private void changelanguage() {
        tvEmail.setText(modelLanguageDTO.getForgot_password());
        RBSeeker.setText(modelLanguageDTO.getLogin_option1());
        RBRecruiter.setText(modelLanguageDTO.getLogin_option2());
        tvyouremail.setText(modelLanguageDTO.getEmail_login());
    //   btnSubmit.setText();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                submitForm();
                break;

        }
    }

    public void submitForm() {
        if (!ValidateEmail()) {
            return;
        }  else if (!SelectType()) {
            return;
        } else {
            if (NetworkManager.isConnectToInternet(mContext)) {
                updatepass();
            } else {
                ProjectUtils.showToast(mContext, getResources().getString(R.string.internet_connection));
            }
        }
    }


    public boolean ValidateEmail() {
        if (!ProjectUtils.IsEmailValidation(etEmail.getText().toString().trim())) {
            etEmail.setError(modelLanguageDTO.getAll_field_required());
            etEmail.requestFocus();
            return false;
        }
        return true;
    }


    public boolean SelectType() {

        if (radioGroup.getCheckedRadioButtonId() == -1) {
            Toast.makeText(getApplicationContext(), modelLanguageDTO.getAll_field_required(), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public void updatepass() {
        if (RBSeeker.isChecked()) {
            forgotpass();
        }
        if (RBRecruiter.isChecked()) {
            forgotpass();
        }
    }

    public void forgotpass() {
        ProjectUtils.showProgressDialog(mContext, false, "Please wait...");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.FORGOT_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("F RESPONSE",""+response);
                        ProjectUtils.pauseProgressDialog();

                        try {
                            CommonDTO commonDTO = new CommonDTO();
                            commonDTO = new Gson().fromJson(response, CommonDTO.class);

                            if (commonDTO.getStaus().equalsIgnoreCase("true")) {
                                ProjectUtils.showToast(ForgotPasswordActivity.this, commonDTO.getMessage());
                            } else {
                                ProjectUtils.showToast(ForgotPasswordActivity.this, commonDTO.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProjectUtils.pauseProgressDialog();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (RBRecruiter.isChecked()) {
                    params.put(AppConstans.TYPE, "recruiter");

                } else if (RBSeeker.isChecked()) {
                    params.put(AppConstans.TYPE, "seeker");
                }
                params.put(AppConstans.EMAIL, ProjectUtils.getEditTextValue(etEmail));
                Log.e("FG VALUE",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }
}
