package com.pixelnx.sam.jobportal.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Activity.RecruiterDashboardActivity;
import com.pixelnx.sam.jobportal.Activity.Recruiter_PostJob_Activity;
import com.pixelnx.sam.jobportal.Activity.SingleJob;
import com.pixelnx.sam.jobportal.Adapter.JobApplication_adapter;
import com.pixelnx.sam.jobportal.DTO.JobApplicationDTO;
import com.pixelnx.sam.jobportal.DTO.UserRecruiterDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterPostJobs;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class JobApplication extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private ArrayList<ModelRecruiterPostJobs.Data> jobApplicationDTOList = new ArrayList<>();
    private ModelRecruiterPostJobs jobApplicationDTO;
    private JobApplication_adapter jobApplication_adapter;
    private UserRecruiterDTO.Data data;
    public LinearLayout llIcon;
    // private RecruiterProfileDTO recruiterProfileDTO;
    View view;
    private RecyclerView rvJobAppliction;
    private ImageView fab_image;
    RecruiterDashboardActivity recruiterDashboardActivity;
    private SharedPrefrence sharedPrefrence;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AdView mAdView;
    private View adMobView;
    private LinearLayout llSearchR;
    private CustomEdittext etSearch;
    private ImageView ivSearch;
    InputMethodManager inputManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_job_application, container, false);
        inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        sharedPrefrence = SharedPrefrence.getInstance(getActivity());
        recruiterDashboardActivity.tvJobs.setVisibility(View.VISIBLE);
        recruiterDashboardActivity.search_icon.setVisibility(View.VISIBLE);
        recruiterDashboardActivity.filter_icon.setVisibility(View.GONE);
        init(view);
        return view;
    }

    private void init(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        llIcon = (LinearLayout) view.findViewById(R.id.llIcon);
        rvJobAppliction = (RecyclerView) view.findViewById(R.id.rvJobAppliction);
        fab_image = (ImageView) view.findViewById(R.id.fab_image);

        llSearchR = (LinearLayout) view.findViewById(R.id.llSearchR);
        etSearch = (CustomEdittext) view.findViewById(R.id.etSearch);
        ivSearch = (ImageView) view.findViewById(R.id.fab_image);


        adMobView = view.findViewById(R.id.adMobView);
        fab_image.setOnClickListener(this);
        try {
            recruiterDashboardActivity.tvJobs.setText(sharedPrefrence.getLanguage(AppConstans.LANGUAGE).getData().getSingle_job_post_date());
        } catch (Exception e) {
            e.printStackTrace();

        }
        //ProjectUtils.showAdd(getActivity(), mAdView,"ca-app-pub-3940256099942544/6300978111", adMobView);
       /* if(sharedPrefrence.getUserDTO(AppConstans.SEEKERDTO).getAdmob().equalsIgnoreCase(""))
        {
            ProjectUtils.showAdd(getActivity(), mAdView,"ca-app-pub-3940256099942544/6300978111", adMobView);
        }*/

        // Capture Text in EditText
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
                String text = etSearch.getText().toString().toLowerCase();
                if (jobApplicationDTOList.size() != 0)
                    jobApplication_adapter.filter(text);
            }
        });

        recruiterDashboardActivity.search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llSearchR.getVisibility() == View.VISIBLE) {
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    recruiterDashboardActivity.search_icon.setImageResource(R.drawable.search);
                    llSearchR.setVisibility(View.GONE);
                    //searchJobByKeyWord(etSearch.getText().toString());
                } else {
                    recruiterDashboardActivity.search_icon.setImageResource(R.drawable.close);
                    etSearch.requestFocus();
                    inputManager.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
                    llSearchR.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void showPostedJobs() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.RECRUITER_POST_JOBS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            jobApplicationDTO = new Gson().fromJson(response, ModelRecruiterPostJobs.class);
                            if (jobApplicationDTO.getStaus().equalsIgnoreCase("true")) {
                                jobApplicationDTOList = jobApplicationDTO.getData();
                                LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                linearLayoutManagerVertical.supportsPredictiveItemAnimations();
                                rvJobAppliction.setLayoutManager(linearLayoutManagerVertical);
                                if (jobApplicationDTOList.size() != 0) {
                                    rvJobAppliction.setVisibility(View.VISIBLE);
                                    llIcon.setVisibility(View.GONE);
                                    jobApplication_adapter = new JobApplication_adapter(jobApplicationDTOList, getActivity(), JobApplication.this);
                                    rvJobAppliction.setAdapter(jobApplication_adapter);
                                } else {
                                    rvJobAppliction.setVisibility(View.GONE);
                                    llIcon.setVisibility(View.VISIBLE);
                                }
                            } else {
                                rvJobAppliction.setVisibility(View.GONE);
                                llIcon.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstans.EMAIL, sharedPrefrence.getUserDTO(AppConstans.SEEKERDTO).getData().getEmail());
                Log.e("value", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        Log.e("Runnable", "FIRST");
                                        if (NetworkManager.isConnectToInternet(getActivity())) {
                                            swipeRefreshLayout.setRefreshing(true);
                                            showPostedJobs();

                                        } else {
                                            ProjectUtils.showToast(getActivity(), getResources().getString(R.string.internet_connection));
                                        }
                                    }
                                }
        );


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_image:
                Intent in = new Intent(getActivity(), Recruiter_PostJob_Activity.class);
                in.putExtra(Consts.FLAG, 1);
                startActivity(in);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        recruiterDashboardActivity = (RecruiterDashboardActivity) activity;
    }

    @Override
    public void onRefresh() {
        showPostedJobs();
    }
}
