package com.pixelnx.sam.jobportal;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.pixelnx.sam.jobportal.Fragment.Recruiter_Reg_Fragment;
import com.pixelnx.sam.jobportal.Fragment.Seeker_Reg_Fragment;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.CustomTextSubHeader;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvSeeker, tvRecruiter;
    private FragmentManager fm;
    private AdView mAdView;
    private Seeker_Reg_Fragment seekerRegFragment = new Seeker_Reg_Fragment();
    private Recruiter_Reg_Fragment recruiterFragment = new Recruiter_Reg_Fragment();
    private View adMobView;
    private SharedPrefrence pref;
    private CustomTextSubHeader tvRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.frame, seekerRegFragment, "").commit();
        tvSeeker = (TextView) findViewById(R.id.tvSeeker);
        tvRecruiter = (TextView) findViewById(R.id.tvRecruiter);
        tvRegister = (CustomTextSubHeader) findViewById(R.id.tvRegister);
        //  mAdView = (AdView) findViewById(R.id.adView);
        adMobView = findViewById(R.id.adMobView);
        tvSeeker.setOnClickListener(this);
        tvRecruiter.setOnClickListener(this);
        tvSeeker.setTextColor(Color.rgb(106, 100, 231));

        tvRecruiter.setTextColor(Color.rgb(204, 202, 247));
        ProjectUtils.showAdd(RegisterActivity.this, mAdView, "ca-app-pub-3940256099942544/6300978111", adMobView);

        pref = SharedPrefrence.getInstance(RegisterActivity.this);
        tvRecruiter.setText(pref.getLanguage(AppConstans. LANGUAGE).getData().getLogin_option2());
        tvSeeker.setText(pref.getLanguage(AppConstans. LANGUAGE).getData().getLogin_option1());
        tvRegister.setText(pref.getLanguage(AppConstans. LANGUAGE).getData().getRegister_keyword());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSeeker:
                fm.beginTransaction().replace(R.id.frame, seekerRegFragment, "").commit();
                tvSeeker.setTextColor(Color.rgb(106, 100, 231));
                tvRecruiter.setTextColor(Color.rgb(204, 202, 247));
                break;
            case R.id.tvRecruiter:
                fm.beginTransaction().replace(R.id.frame, recruiterFragment, "").commit();
                tvSeeker.setTextColor(Color.rgb(204, 202, 247));
                tvRecruiter.setTextColor(Color.rgb(106, 100, 231));
                break;
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

}
