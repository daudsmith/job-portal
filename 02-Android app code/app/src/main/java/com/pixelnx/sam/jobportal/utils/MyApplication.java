package com.pixelnx.sam.jobportal.utils;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;
import com.pixelnx.sam.jobportal.R;

/**
 * Created by yash on 18/01/2018.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize the AdMob app
        MobileAds.initialize(this, getString(R.string.admob_app_id));
    }
}
