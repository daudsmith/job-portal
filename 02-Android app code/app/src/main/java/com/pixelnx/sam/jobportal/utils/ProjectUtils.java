package com.pixelnx.sam.jobportal.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.pixelnx.sam.jobportal.DTO.DummyFilterDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelPlanDTO;
import com.pixelnx.sam.jobportal.Fragment.Recruiter_Reg_Fragment;
import com.pixelnx.sam.jobportal.LoginActivity;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.payment.PayMentGateWay;
import com.pixelnx.sam.jobportal.payment.paypal.PayPalActivity;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static io.fabric.sdk.android.Fabric.TAG;


/**
 * Created by varun on 27/2/17.
 */
public class ProjectUtils {
  /*  private static ImageView d_img;
    private static    AnimationDrawable anim;*/
    private static Toast toast;
    private static ProgressDialog mProgressDialog;
    private static AlertDialog dialog;
    public static final Calendar refCalender = Calendar.getInstance();
    public static final Calendar myCalendar = Calendar.getInstance();
    public static Date date;
    public static HashMap<Integer, ArrayList<DummyFilterDTO>> map = new HashMap<>();
    public static boolean IsEditTextValidation(EditText text) {
        if (text.getText() != null && text.getText().toString().trim().length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean IsEditTextYear(EditText text) {
        if (text.getText() != null && text.getText().toString().trim().length() == 4) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean IsEditTextPercent(EditText text) {
        String regex = "\\d+(?:\\\\.\\\\d+)?%";
        if (text.getText() != null && text.getText().toString().trim().length() <= 3 && text.getText().toString().trim().matches(regex) == true) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean IsPasswordValidation(String password) {
        if (password.length() < 4 || password.length() >= 13) {
            return false;
        }
        return true;
    }

    public static boolean IsEmailValidation(String email) {

        String regex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            return true;
        } else if (email.equals("")) {
            return false;
        }
        return false;
    }

    public static boolean IsMobleValidation(String mobile) {

        String regexStr = "^((0)|(91)|(00)|[7-9]){1}[0-9]{3,14}$";
        if (mobile.length() < 10 || mobile.length() > 13 || mobile.matches(regexStr) == false) {
            return false;
        }
        return true;
    }


    public static boolean hasPermissionInManifest(Activity activity, int requestCode, String permissionName) {
        if (ContextCompat.checkSelfPermission(activity,
                permissionName)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{permissionName},
                    requestCode);
        } else {
            return true;
        }
        return false;
    }

    public static Dialog showProgressDialog(Context context, boolean isCancelable, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
        mProgressDialog.setCancelable(true);
        return mProgressDialog;
    }

    public static void pauseProgressDialog() {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.cancel();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    public static void showToast(Context context, String message) {
        if (message == null) {
            return;
        }
        if (toast == null && context != null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        }
        if (toast != null) {
            toast.setText(message);
            toast.show();
        }
    }

    public static String getEditTextValue(EditText text) {
        return text.getText().toString().trim();
    }

    public static void initImageLoader(Context context) {

        File cacheDir = StorageUtils.getOwnCacheDirectory(context, Consts.JOB_PORTAL);
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCache(new UnlimitedDiskCache(cacheDir));
        config.diskCacheSize(50 * 1024 * 1024);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs();


        ImageLoader.getInstance().init(config.build());


    }

    /**
     * This method will create alert dialog
     *
     * @param context  Context of calling class
     * @param title    Title of the dialog to be shown
     * @param msg      Msg of the dialog to be shown
     * @param btnText  array of button texts
     * @param listener
     */
    public static void showAlertDialog(Context context, String title,
                                       String msg, String btnText, String btnCancel,
                                       DialogInterface.OnClickListener listener,
                                       DialogInterface.OnClickListener listenerCancel) {

        if (listener == null)
            listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface,
                                    int paramInt) {
                    paramDialogInterface.dismiss();
                    paramDialogInterface.dismiss();
                }
            };
        if (listenerCancel == null)
            listenerCancel = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface,
                                    int paramInt) {
                    paramDialogInterface.dismiss();
                    paramDialogInterface.dismiss();
                }
            };

        Builder builder = new Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(btnText, listener);
        builder.setNegativeButton(btnCancel, listenerCancel);
        dialog = builder.create();
        dialog.setCancelable(true);
        try {
            dialog.show();
        } catch (Exception e) {
            // TODO: handle exception
        }

    }
    public static void showAlertDialogOne(Context context, String title,
                                       String msg, String btnText,
                                       DialogInterface.OnClickListener listener) {

        if (listener == null)
            listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface,
                                    int paramInt) {
                    paramDialogInterface.dismiss();
                    paramDialogInterface.dismiss();
                }
            };

        Builder builder = new Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(btnText, listener);
        dialog = builder.create();
        dialog.setCancelable(true);
        try {
            dialog.show();
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public static void datePicker(final Calendar calendar, final Context context, final EditText editText, final boolean code) {

        int year = calendar.get(Calendar.YEAR);
        int monthOfYear = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                calendar.set(Calendar.YEAR, y);
                calendar.set(Calendar.MONTH, m);
                calendar.set(Calendar.DAY_OF_MONTH, d);
                if (code) {
                    if (calendar.getTimeInMillis() >= refCalender.getTimeInMillis()) {
                        String myFormat = "dd/MM/yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        editText.setText(sdf.format(calendar.getTime()));
                    } else {
                        ProjectUtils.showToast(context, "Please select correct date.");
                    }

                } else {

                    if (calendar.getTimeInMillis() <= refCalender.getTimeInMillis()) {
                        String myFormat = "yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        editText.setText(sdf.format(calendar.getTime()));
                    } else {
                        ProjectUtils.showToast(context, "Cannot select future date");
                    }

                }
            }


        }, year, monthOfYear, dayOfMonth);
        datePickerDialog.setTitle("Select Date");
        if (code) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        } else {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        datePickerDialog.show();
    }


    public static void timePicker(final Calendar calendar, final Context context, final EditText editText) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                calendar.set(Calendar.MINUTE, selectedMinute);

                SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
                String currentTime = sdf1.format(new Date());
                editText.setText(sdf1.format(calendar.getTime()));
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public static void showSweetDialog(Context context, String title,
                                       String msg, String btnText,
                                       SweetAlertDialog.OnSweetClickListener listener, int dialogType) {

        if (listener == null)
            listener = new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();
                }
            };
        SweetAlertDialog alertDialog = new SweetAlertDialog(context, dialogType);
        alertDialog.setTitleText(title);
        alertDialog.setConfirmClickListener(listener);
        alertDialog.setContentText(msg);
        alertDialog.setConfirmText(btnText);
        try {
           /* TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP,17);
            text.setTypeface(customFont);
            text.setGravity(Gravity.CENTER);*/
            alertDialog.show();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    public static void dateFormate(TextView textView, String txt) {
        DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

        DateFormat writeFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        try {
            date = readFormat.parse(txt);
            Log.e("Date one", "" + date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            String formattedDate = writeFormat.format(date);
            textView.setText(formattedDate);
        }
    }

    public static void dateFormateOne(TextView textView, String txt) { //"1970-01-01"

        DateFormat readFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        DateFormat writeFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        try {
            date = readFormat.parse(txt);
            Log.e("Date one", "" + date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            String formattedDate = writeFormat.format(date);
            textView.setText(formattedDate);
        }
    }
    public static void showAdd(Context context,AdView mAdView,String appID,View adContainer) {

     mAdView=new AdView(context);
        ((RelativeLayout)adContainer).addView(mAdView);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(appID);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

    }
    public static void showDialog(final Activity activity, String email, String mobileNo) {
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setTitle("Payment Option");
        dialog.setContentView(R.layout.pdf_pic_dialog);

        CustomButton btCloase=(CustomButton)dialog.findViewById(R.id.btCloase) ;
        getPlanDetails(dialog, activity,email,mobileNo);


        btCloase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              activity.finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

   /* public static void startLoader(Activity activity){
      if(!(anim!=null && d_img !=null)){
          return;
      }
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.show_loader);


        d_img=(ImageView)dialog.findViewById(R.id.d_img) ;

        d_img.setVisibility(View.VISIBLE);
        anim = (AnimationDrawable) d_img.getDrawable();
        d_img.post(run);


        dialog.show();
    }
   static Runnable run = new Runnable() {
        @Override
        public void run() {
            anim.start();
        }
    };

    public static void stopLoader() {
        if (anim != null) {
            anim.stop();
            d_img.setVisibility(View.GONE);
        }
    }*/
    private static void getPlanDetails(Dialog dialog, final Activity activity, final String email, final String mobileNo) {
        final ListView mListView;
        mListView = (ListView) dialog.findViewById(R.id.listAttachments);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.PLAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ProjectUtils.pauseProgressDialog();
                        Log.e(TAG, "response:" + response);
                        ModelPlanDTO modelPlanDTO = new ModelPlanDTO();
                        modelPlanDTO = new Gson().fromJson(response, ModelPlanDTO.class);
                        if (modelPlanDTO.getStaus().equalsIgnoreCase("true")) {
                            ArrayList<ModelPlanDTO.Data> data = new ArrayList<>();
                            data = modelPlanDTO.getData();
                            PlanAdapter planAdapter = new PlanAdapter(data,activity,email,mobileNo);
                            mListView.setAdapter(planAdapter);
                        } else {

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProjectUtils.pauseProgressDialog();
                        Log.e("error_requter", error.toString());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    public static class PlanAdapter extends BaseAdapter {
        ArrayList<ModelPlanDTO.Data> data;
        Activity activity;
        String email="",mobileNo="";
        public PlanAdapter(ArrayList<ModelPlanDTO.Data> data,Activity activity,String email,String mobileNo) {

            this.data = data;
            this.activity=activity;
            this.mobileNo=mobileNo;
            this.email=email;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            final ViewHolder holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            View itemView = inflater.inflate(R.layout.adapter_plan_type, null);
            holder.tvPayu = (CustomTextviewBold) itemView.findViewById(R.id.tvPayu);
            holder.tvPayPal = (CustomTextviewBold) itemView.findViewById(R.id.tvPayPal);
            holder.tvPlanName = (CustomTextviewBold) itemView.findViewById(R.id.tvPlanName);
            holder.tvAmount = (CustomTextviewBold) itemView.findViewById(R.id.tvAmount);
            holder.tvDuration = (CustomTextview) itemView.findViewById(R.id.tvDuration);
            holder.tvDescription = (CustomTextview) itemView.findViewById(R.id.tvDescription);
            holder.cardClick = (CardView) itemView.findViewById(R.id.cardClick);

            holder.tvPlanName.setText(data.get(i).getName());
            holder.tvAmount.setText(data.get(i).getAmount_inr());
            holder.tvDescription.setText(data.get(i).getDescription());
            holder.tvDuration.setText(data.get(i).getDuration());

            holder.tvPayu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, PayMentGateWay.class);
                    intent.putExtra(AppConstans.MOBILE_NO, mobileNo);
                    intent.putExtra(AppConstans.AMOUNT, data.get(i).getAmount_inr());
                    intent.putExtra(AppConstans.EMAIL, email);
                    activity. startActivity(intent);
                }
            });

            holder.tvPayPal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent intent = new Intent(activity, PayPalActivity.class);
                    intent.putExtra(AppConstans.AMOUNT, data.get(i).getAmount_usd());
                    intent.putExtra(AppConstans.EMAIL, email);
                    intent.putExtra(AppConstans.PLAN_ID, data.get(i).getId());
                    activity. startActivity(intent);
                }
            });
            return itemView;
        }


    }

    static class ViewHolder {
        public CustomTextviewBold tvPlanName, tvAmount,tvPayPal,tvPayu;
        public CustomTextview tvDuration, tvDescription;
        public CardView cardClick;
    }
}