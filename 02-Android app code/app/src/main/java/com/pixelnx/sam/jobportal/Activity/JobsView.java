package com.pixelnx.sam.jobportal.Activity;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Adapter.JobViewAdapter;
import com.pixelnx.sam.jobportal.DTO.JobsViewDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelSeekerLogin;
import com.pixelnx.sam.jobportal.DTOCI.ModelSingleJobDisplay;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomTextHeader;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JobsView extends AppCompatActivity {
    private RecyclerView rvJobsView;
    private JobsViewDTO jobsViewDTO;
    private ArrayList<JobsViewDTO.Data> dataList = new ArrayList<>();
    private Context mContext;
    private JobViewAdapter jobViewAdapter;
    private CustomTextHeader tvJobs;
    private String jobid, recid, actionBarText;
    private ImageView ivBack, img;
    private AnimationDrawable anim;
    private ModelSeekerLogin userSeekerDTO;
    private SharedPrefrence prefrence;
    private AdView mAdView;
    private View adMobView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_view);
        mContext = JobsView.this;
        prefrence=SharedPrefrence.getInstance(mContext);
        userSeekerDTO = prefrence.getUserDTO(AppConstans.SEEKERDTO);
        jobid = getIntent().getExtras().getString(Consts.JOB_ID);
        recid = getIntent().getExtras().getString(Consts.RECRUITER_ID);
        actionBarText = getIntent().getExtras().getString(Consts.ET_NAME);
        init();
        tvJobs.setText("" + actionBarText);
    }

    private void init() {
        rvJobsView = (RecyclerView) findViewById(R.id.rvJobsView);
        tvJobs = (CustomTextHeader) findViewById(R.id.tvJobs);
        img = (ImageView) findViewById(R.id.img);
        adMobView = findViewById(R.id.adMobView);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        getJobDetails();
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        if (!userSeekerDTO.getAdmob().equalsIgnoreCase("")){
            ProjectUtils.showAdd(JobsView.this, mAdView, userSeekerDTO.getAdmob(), adMobView);
        }
    }


    private void getJobDetails() {
        Animation();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.VIEW_APPLIED_SEEKER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        stopAnimation();

                        try {
                            jobsViewDTO = new JobsViewDTO();
                            jobsViewDTO = new Gson().fromJson(response, JobsViewDTO.class);
                            stopAnimation();
                            if (jobsViewDTO.getStaus().equalsIgnoreCase("true")) {
                                showJobs(jobsViewDTO);
                            } else {
                                ProjectUtils.showToast(JobsView.this, jobsViewDTO.getMessage());
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("staus").equalsIgnoreCase("false")) {
                                    ProjectUtils.showToast(mContext, jsonObject.getString("Message"));
                                }
                            } catch (Exception ef) {
ef.printStackTrace();
                            }
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        stopAnimation();
                        ProjectUtils.showToast(mContext, error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //  params.put(Consts.SEEKER_ID, userSeekerDTO.getData().getId());
                params.put(AppConstans.JOB_ID, jobid);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(JobsView.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }


    private void showJobs(JobsViewDTO jobsViewDTO) {
        dataList = jobsViewDTO.getData();
        LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        linearLayoutManagerVertical.supportsPredictiveItemAnimations();
        rvJobsView.setLayoutManager(linearLayoutManagerVertical);

        jobViewAdapter = new JobViewAdapter(dataList, mContext, jobid);
        rvJobsView.setAdapter(jobViewAdapter);
    }


    private void Animation() {
        img.setVisibility(View.VISIBLE);
        anim = (AnimationDrawable) img.getDrawable();
        img.post(run);
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {
            anim.start();
        }
    };

    private void stopAnimation() {
        anim.stop();
        img.setVisibility(View.GONE);
    }
}
