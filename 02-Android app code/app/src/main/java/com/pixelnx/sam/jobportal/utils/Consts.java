package com.pixelnx.sam.jobportal.utils;

/**
 * Created by shubham on 19/7/17.
 */

public interface Consts {


    /*
    * Please Don't Use this  Const file because it is not usable in JPV2
    *
    * */





    public static String JOB_PORTAL = "JobPortal";

    //public static String BASE_URL = "http://jobportal.kamleshyadav.net/admin/api/";
    public static String BASE_URL = "http://pixelpages.net/job_portal/api/";
    public static String IS_REGISTER_SEEKER = "is_register_seeker";
    public static String IS_REGISTER_RECRUITER = "is_register_recruiter";
    static String SECONDLANGUAGE = "hi";//put you language code here

    public static String FILL_SEEKER_PROFILE = "fill-seeker-profile";


    public static String SEEKER_DTO = "seeker_dto";
    public static String ET_NAME = "et_name";



    String IMAGE_URI_CAMERA = "image_uri_camera";
    String FLAG = "flag";
    String TOKAN = "tokan";
    String TAG_GUEST = "guest";
    String SINGLE_JOB_DTO = "singleJobDTO";



    String EMAIL = "email";




    String VALUE = "value";


    String SEEKER_ID = "seeker_id";
    String JOB_ID = "job_id";

    String JOB_TYPE = "job_type";
    String AREA_OF_SECTOR = "area_of_sector";
    String SPECIALIZATION = "specialization";

    String RECRUITER_ID = "recruiter_id";


     String ENGLIShLANGUAGE= "en";

    String JOB_BY_ROLES = "job_by_roles";
    String QUALIFICATION = "qualification";
    String JOB_LOCATION = "job_location";

    String EXPERIENCE = "experience";

    String CAMERA_ACCEPTED = "camera_accepted";
    String STORAGE_ACCEPTED = "storage_accepted";
    String NETWORK_ACCEPTED = "network_accepted";
    String CALL_PR_ACCEPTED = "call_pr_accepted";
    String CALL_PH_ACCEPTED = "call_ph_accepted";
    String READ_PH_ACCEPTED = "read_ph_accepted";
    String ANOTHER_LANGUAGE="another_language";
}
