package com.pixelnx.sam.jobportal.DTO;

import java.io.Serializable;

/**
 * Created by shubham on 30/8/17.
 */

public class SingleJobDTO implements Serializable {

  String staus="";
        String message = "";
    Data data;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {
        String id = "";
        String name = "";
        String email = "";
        String mno = "";
        String gender = "";
        String current_address = "";
        String p_locaion = "";
        String job_type = "";
        String qua = "";
        String p_year = "";
        String cgpa = "";
        String aofs = "";
        String exp = "";
        String resume = "";
        String veri = "";
        String img = "";
        String counter = "";
        String status = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMno() {
            return mno;
        }

        public void setMno(String mno) {
            this.mno = mno;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCurrent_address() {
            return current_address;
        }

        public void setCurrent_address(String current_address) {
            this.current_address = current_address;
        }

        public String getP_locaion() {
            return p_locaion;
        }

        public void setP_locaion(String p_locaion) {
            this.p_locaion = p_locaion;
        }

        public String getJob_type() {
            return job_type;
        }

        public void setJob_type(String job_type) {
            this.job_type = job_type;
        }

        public String getQua() {
            return qua;
        }

        public void setQua(String qua) {
            this.qua = qua;
        }

        public String getP_year() {
            return p_year;
        }

        public void setP_year(String p_year) {
            this.p_year = p_year;
        }

        public String getCgpa() {
            return cgpa;
        }

        public void setCgpa(String cgpa) {
            this.cgpa = cgpa;
        }

        public String getAofs() {
            return aofs;
        }

        public void setAofs(String aofs) {
            this.aofs = aofs;
        }

        public String getExp() {
            return exp;
        }

        public void setExp(String exp) {
            this.exp = exp;
        }

        public String getResume() {
            return resume;
        }

        public void setResume(String resume) {
            this.resume = resume;
        }

        public String getVeri() {
            return veri;
        }

        public void setVeri(String veri) {
            this.veri = veri;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getCounter() {
            return counter;
        }

        public void setCounter(String counter) {
            this.counter = counter;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

}
