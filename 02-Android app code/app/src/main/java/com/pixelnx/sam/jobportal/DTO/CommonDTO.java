package com.pixelnx.sam.jobportal.DTO;


import java.io.Serializable;

/**
 * Created by shubham on 16/8/17.
 */

public class CommonDTO implements Serializable {
    String  staus ="" ;
    String message = "";

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
