package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;
import java.util.ArrayList;

public class FetchGeneralData implements Serializable {

    String staus = "";
    String message = "";
    Data data;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data implements Serializable {

   ArrayList<Location> location=new ArrayList<>();
   ArrayList<JobType> job_type=new ArrayList<>();
   ArrayList<Qualification> qualification=new ArrayList<>();
   ArrayList<Exp> exp=new ArrayList<>();
   ArrayList<Desi> desi=new ArrayList<>();
   ArrayList<Area_of_s> area_of_s=new ArrayList<>();
   ArrayList<JobRole> job_role=new ArrayList<>();
   ArrayList<Specialization> specialization=new ArrayList<>();


        public ArrayList<Location> getLocation() {
            return location;
        }

        public void setLocation(ArrayList<Location> location) {
            this.location = location;
        }

        public ArrayList<JobType> getJob_type() {
            return job_type;
        }

        public void setJob_type(ArrayList<JobType> job_type) {
            this.job_type = job_type;
        }

        public ArrayList<Qualification> getQualification() {
            return qualification;
        }

        public void setQualification(ArrayList<Qualification> qualification) {
            this.qualification = qualification;
        }

        public ArrayList<Exp> getExp() {
            return exp;
        }

        public void setExp(ArrayList<Exp> exp) {
            this.exp = exp;
        }

        public ArrayList<Desi> getDesi() {
            return desi;
        }

        public void setDesi(ArrayList<Desi> desi) {
            this.desi = desi;
        }

        public ArrayList<Area_of_s> getArea_of_s() {
            return area_of_s;
        }

        public void setArea_of_s(ArrayList<Area_of_s> area_of_s) {
            this.area_of_s = area_of_s;
        }

        public ArrayList<JobRole> getJob_role() {
            return job_role;
        }

        public void setJob_role(ArrayList<JobRole> job_role) {
            this.job_role = job_role;
        }

        public ArrayList<Specialization> getSpecialization() {
            return specialization;
        }

        public void setSpecialization(ArrayList<Specialization> specialization) {
            this.specialization = specialization;
        }

        public static class Location implements Serializable {
         String id="";
         String name="";
         String status="";

            public Location(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @Override
            public String toString() {
                return name.toString();
            }
        }

        public static class JobType implements Serializable {
            String id="";
            String name="";
            String status="";

            public JobType(String name) {
                this.name = name;
            }
            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @Override
            public String toString() {
                return name.toString();
            }
        }

        public static class Qualification implements Serializable {
            String id="";
            String name="";
            String status="";
            public Qualification(String name) {
                this.name = name;
            }
            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
            @Override
            public String toString() {
                return name.toString();
            }
        }

        public static class Exp implements Serializable {
            String id="";
            String name="";
            String status="";

            public Exp(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
            @Override
            public String toString() {
                return name.toString();
            }
        }

        public static class Desi implements Serializable {
            String id="";
            String name="";
            String status="";

            public Desi(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }


            @Override
            public String toString() {
                return  name.toString();
            }
        }

        public static class Area_of_s implements Serializable {
            String id="";
            String name="";
            String status="";
            public Area_of_s(String name) {
                this.name = name;
            }
            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @Override
            public String toString() {
                return name.toString();
            }
        }

        public static class JobRole implements Serializable {
            String id="";
            String name="";
            String status="";
            public JobRole(String name) {
                this.name = name;
            }
            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
            @Override
            public String toString() {
                return name.toString();
            }
        }

        public static class Specialization implements Serializable {
            String id="";
            String name="";
            String status="";

            public Specialization(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @Override
            public String toString() {
                return name.toString();
            }
        }

    }

}
