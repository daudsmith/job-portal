package com.pixelnx.sam.jobportal.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.DTO.SingleJobDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelSeekerLogin;
import com.pixelnx.sam.jobportal.LoginActivity;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomTextHeader;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
@SuppressWarnings("deprecation")

public class SingleJob extends AppCompatActivity implements View.OnClickListener {
    private CustomTextviewBold tvName, tvResume;
    private CustomTextview tvExperience, tvLocation, tvSkills, tvJobApplied, tvJobtype, tvDesignation, tvQualification, tvlastDOAPP, tvJobDescription, tvSpecialization;
    // public JobsViewDTO.Data.Profile profile;
    private ImageView ivBack;
    private CustomTextHeader tvHeader;
    private CustomButton btnCallNow;
    private SingleJobDTO singleJobDTO;
    private String seeker_id, jobid;
    private Context mContext;
    private ProgressDialog pDialog;
    private AdView mAdView;
    private   View adMobView;
private SharedPrefrence prefrence;
    ImageView my_image;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    static String storagestate = Environment.getExternalStorageState();
    private static File imageFilepath;
    String extensionResume = "";
     String path="";
private ModelSeekerLogin modelSeekerLogin;
    private ModelLanguageDTO.Data modelLanguageDTO;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rui_c);
        mContext = SingleJob.this;
        prefrence=SharedPrefrence.getInstance(mContext);
        modelSeekerLogin=prefrence.getUserDTO(AppConstans.SEEKERDTO);
        modelLanguageDTO=prefrence.getLanguage(AppConstans.LANGUAGE).getData();
        seeker_id = getIntent().getExtras().getString(Consts.SEEKER_ID);
        jobid = getIntent().getExtras().getString(Consts.JOB_ID);
        init();


    }

    private void init() {
        tvHeader = (CustomTextHeader) findViewById(R.id.tvHeader);

        tvName = (CustomTextviewBold) findViewById(R.id.tvName);
        tvExperience = (CustomTextview) findViewById(R.id.tvExperience);
        tvLocation = (CustomTextview) findViewById(R.id.tvLocation);
        tvJobApplied = (CustomTextview) findViewById(R.id.tvJobApplied);
        tvSkills = (CustomTextview) findViewById(R.id.tvSkills);
        tvJobtype = (CustomTextview) findViewById(R.id.tvJobtype);
        tvDesignation = (CustomTextview) findViewById(R.id.tvDesignation);
        tvQualification = (CustomTextview) findViewById(R.id.tvQualification);
        tvlastDOAPP = (CustomTextview) findViewById(R.id.tvlastDOAPP);
        tvSpecialization = (CustomTextview) findViewById(R.id.tvSpecialization);
        tvResume = (CustomTextviewBold) findViewById(R.id.tvResume);
        tvJobDescription = (CustomTextview) findViewById(R.id.tvJobDescription);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        btnCallNow = (CustomButton) findViewById(R.id.btnCallNow);

        adMobView = findViewById(R.id.adMobView);
       // mAdView = (AdView) findViewById(R.id.adView);

        changeLanguage();

        btnCallNow.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        tvResume.setOnClickListener(this);
        viewProfile();
        if (!modelSeekerLogin.getAdmob().equals(""))
        ProjectUtils.showAdd(SingleJob.this, mAdView, modelSeekerLogin.getAdmob(), adMobView);
    }

    private void changeLanguage() {

        CustomTextviewBold lgjobType,lgjQua,lgyearp,tvResume;

        lgjobType = (CustomTextviewBold) findViewById(R.id.lgjobType);
        tvResume = (CustomTextviewBold) findViewById(R.id.tvResume);
        lgjQua = (CustomTextviewBold) findViewById(R.id.lgjQua);
        lgyearp = (CustomTextviewBold) findViewById(R.id.lgyearp);

        lgjobType.setText(modelLanguageDTO.getJob_type());
        lgjQua.setText(modelLanguageDTO.getQua());
        lgyearp.setText(modelLanguageDTO.getP_year());
        tvResume.setText(modelLanguageDTO.getResume_keyword());



    }

    private void viewProfile() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.SEEKER_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("Single Job View", response.toString());
                            singleJobDTO = new Gson().fromJson(response, SingleJobDTO.class);
                            if (singleJobDTO.getStaus().equalsIgnoreCase("true")) {
                                showDetails(singleJobDTO.getData());
                            } else {
                                ProjectUtils.showToast(mContext, singleJobDTO.getMessage());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstans.SEEKER_EMAIL, seeker_id);
               // params.put(Consts.JOB_ID, jobid);
                Log.e("value", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }


    private void showDetails(SingleJobDTO.Data data) {

path=data.getResume();
Log.e("RESUME",path);
        tvHeader.setText(data.getName());
        tvName.setText(data.getName());
        if (data.getExp().equalsIgnoreCase("Fresher")) {
            tvExperience.setText(data.getExp());

       } else {
          tvExperience.setText(data.getExp());
          }

        if (data.getResume().equals("")) {
            tvResume.setVisibility(View.GONE);
        }

        tvJobtype.setText(data.getJob_type());
        tvLocation.setText(data.getP_locaion());
        tvSpecialization.setText(data.getP_year());

        //  tvJobApplied.setText(profile.getSeeker_apply_date());
        tvQualification.setText(data.getQua());


        //ProjectUtils.dateFormate(tvJobApplied, data.getSeeker_apply_date());
    }

    @Override
    public void onBackPressed() {
        //    super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.tvResume:
               /* ProjectUtils.showAlertDialog(mContext, "Please Confirm?", getResources().getString(R.string.download_dialog), "View", "Download", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent in = new Intent(mContext, PdfView.class);
                        in.putExtra(Consts.SINGLE_JOB_DTO, singleJobDTO);
                        startActivity(in);
                        dialogInterface.dismiss();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String filenameArray[] = singleJobDTO.getData().getResume().split("\\.");
                        extensionResume = filenameArray[filenameArray.length - 1];
                        new DownloadFileFromURL().execute(singleJobDTO.getData().getProfile().getResume());
                        dialogInterface.dismiss();
                    }
                });*/
               downloadResume();
                break;
            case R.id.btnCallNow:
                try {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + singleJobDTO.getData().getMno() + ""));
                    startActivity(callIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private void downloadResume() {

        //External directory path to save file
     String   folder = Environment.getExternalStorageDirectory() + File.separator + "JPRESUME/";

        //Create androiddeft folder if it does not exist
        File directory = new File(folder);

        if (!directory.exists()) {
            directory.mkdirs();
        }
        startDownload(folder);
    }

    private void startDownload(final String s) {
        AndroidNetworking.download(modelSeekerLogin.getBase_url()+path, s, tvName.getText().toString() )
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        Log.v("Size " + bytesDownloaded, " Total " + totalBytes);
                        long percent = (bytesDownloaded * 100) / totalBytes;

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                       //productdetailsDTO.setProcess(false);
                        Toast.makeText(mContext, "Downloaded in "+s, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.e("ANError ", "" + error.getErrorBody());
                    }
                });
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);

        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();
                File folder = null;


                String root = Environment.getExternalStorageDirectory().toString();
                folder = new File(root, Consts.JOB_PORTAL);

                if (!folder.exists()) {
                    folder.mkdir();
                }

                String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                        .format(new Date());
//getting filepath
                imageFilepath = new File(folder.getPath() + File.separator + singleJobDTO.getData().getId()
                        + timestamp + "." + extensionResume);


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(imageFilepath);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }


            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));


            super.onProgressUpdate(progress);
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            ProjectUtils.showSweetDialog(mContext, "Download Successfully", "", "Ok", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();


                }
            }, SweetAlertDialog.SUCCESS_TYPE);


        }

    }

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}

