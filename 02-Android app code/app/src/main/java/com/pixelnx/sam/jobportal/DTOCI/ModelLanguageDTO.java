package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;

public class ModelLanguageDTO implements Serializable {
    String staus = "";
    String message = "";
    Data data;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {
        String validaton_password_empty = "";
        String guest_keyword = "";
        String gender = "";
        String male_keyword = "";
        String female_keyword = "";
        String login = "";
        String jobs = "";
        String applied_job = "";
        String profile_setting = "";
        String logout = "";
        String home_page_heading = "";
        String text_filter_placeholder = "";
        String filter_up_side_msg = "";
        String view_btn = "";
        String applied_keyword = "";
        String left_job_type = "";
        String left_location = "";
        String left_designation = "";
        String left_qualification = "";
        String left_experience = "";
        String search_locatiion = "";
        String my_applied_job_headding = "";
        String u_profile_keyword = "";
        String user_personal_details = "";
        String user_name = "";
        String user_mno = "";
        String user_address = "";
        String submit_btn = "";
        String user_search_preferences = "";
        String user_location = "";
        String user_job_type = "";
        String user_qualification = "";
        String user_passing_year = "";
        String user_cgpa = "";
        String user_aofs = "";
        String user_exp = "";
        String user_resume = "";
        String user_resume_download = "";
        String user_password_change = "";
        String user_ps = "";
        String user_cps = "";
        String u_p_save_btn = "";
        String login_heading = "";
        String login_option1 = "";
        String login_option2 = "";
        String email_login = "";
        String password_login = "";
        String forgot_password = "";
        String login_btn = "";
        String register_msg = "";
        String Register_keyword = "";
        String forgot_ps_heading = "";
        String forgot_ps_option1 = "";
        String forgot_ps_option2 = "";
        String forget_ps_email = "";
        String forgot_ps_btn = "";
        String register_page_heading = "";
        String register_email = "";
        String resister_mno = "";
        String register_ps = "";
        String register_cps = "";
        String register_btn = "";
        String login_msg = "";
        String login_keyword = "";
        String register_option1 = "";
        String register_option2 = "";
        String register_name = "";
        String my_jobs1 = "";
        String post_a_job1 = "";
        String r_profile_setting = "";
        String applied_seeker = "";
        String recruiter_home_heading = "";
        String edit_keyword = "";
        String change_logo = "";
        String org_type_title = "";
        String type1 = "";
        String type2 = "";
        String org_name = "";
        String org_location11 = "";
        String org_address1 = "";
        String org_website = "";
        String org_desc = "";
        String pay_detail = "";
        String pay_date = "";
        String plan = "";
        String job_post = "";
        String r_download = "";
        String validity = "";
        String active_new_plan = "";
        String job_post_heading1 = "";
        String job_type = "";
        String desi = "";
        String qua = "";
        String job_location = "";
        String p_year = "";
        String job_cgpa = "";
        String sp = "";
        String job_aofs = "";
        String job_exp = "";
        String num_if_v = "";
        String last_date_of_a = "";
        String tech = "";
        String h_pro = "";
        String job_desc1 = "";
        String s_range = "";
        String min1 = "";
        String max1 = "";
        String meta_desc1 = "";
        String meta_keyword1 = "";
        String aname = "";
        String job_post_btn = "";
        String select_keyword = "";
        String jon_tool_top1 = "";
        String jon_tool_top2 = "";
        String s_written_test_k = "";
        String s_gtoup_d_k = "";
        String s_r_round_k = "";
        String s_hr_round_k = "";
        String per_year1 = "";
        String per_month = "";
        String per_day = "";
        String per_hour = "";
        String edit_job_post_heading = "";
        String edit_job_btn = "";
        String single_form_keyword = "";
        String single_organisation_keyword = "";
        String single_job_post_date = "";
        String single_job_type = "";
        String single_designation = "";
        String single_qualification = "";
        String single_specialization = "";
        String single_application_last_date = "";
        String single_min = "";
        String single_max = "";
        String single_h_pro = "";
        String single_job_desc = "";
        String single_org_d = "";
        String single_type = "";
        String single_location = "";
        String single_website = "";
        String applay_btn = "";
        String already_applied_btn = "";
        String l_back_to_home = "";
        String l_or_keyword = "";
        String validaton_email_empty = "";
        String rec_filter_placeholder = "";
        String validation_valid_email = "";
        String validation_password_short = "";
        String validation_email_veri = "";
        String validation_account_dis = "";
        String validation_login_error = "";
        String validation_name_empty = "";
        String validation_mno_empty = "";
        String validation_cps_empty = "";
        String validation_valid_mno = "";
        String validation_email_exists = "";
        String validation_mno_exists = "";
        String validation_new_ps_send = "";
        String validation_select_plocation = "";
        String validation = "";
        String validation_select_qua = "";
        String validation_cps_short = "";
        String validation_ps_not = "";
        String validation_not_found_msg = "";
        String validation_not_found_msg2 = "";
        String validation_select_aofs = "";
        String validation_enter_address = "";
        String validation_pyeat = "";
        String validation_enter_cgpa = "";
        String validation_v_cgpa = "";
        String validation_select_resume = "";
        String validation_profile_success = "";
        String validation_org_name = "";
        String validation_org_loc = "";
        String validation_org_address = "";
        String validation_website_link = "";
        String validation_org_desc = "";
        String validation_valid_link = "";
        String home_not_avil_msg = "";
        String h_job_not_found = "";
        String h_work_exp = "";
        String h_job_location = "";
        String h_req_skils = "";
        String h_icon = "";
        String h_skils = "";
        String r_back_to_home = "";
        String r_or = "";
        String my_a_applied = "";
        String my_a_view = "";
        String s_tool_tip1 = "";
        String s_tool_tip2 = "";
        String s_tool_tip3 = "";
        String user_p_select = "";
        String r_logout = "";
        String r_applied = "";
        String r_view = "";
        String r_edit = "";
        String r_tool_tip1 = "";
        String r_tool_tip2 = "";
        String r_tool_tip3 = "";
        String r_j__written_test_k = "";
        String r_j__hr_round_k = "";
        String r_j__r_round_k = "";
        String r_j__gtoup_d_k = "";
        String job_post_select = "";
        String job_posted = "";
        String form_k = "";
        String org_k = "";
        String org_type = "";
        String j_location = "";
        String j_website = "";
        String tt1 = "";
        String tt2 = "";
        String tt3 = "";
        String s_change = "";
        String rec_pay_error = "";
        String profile_keyword = "";
        String applied_s_heading = "";
        String a_s_error = "";
        String job_btn = "";
        String ps_not_change = "";
        String c_address = "";
        String pro_not_up = "";
        String only_jpg = "";
        String ppic_not = "";
        String pro_pic_up = "";
        String filter_error = "";
        String applay_success = "";
        String active_new_p = "";
        String applied_seeker_not_found = "";
        String seeker_d_heading = "";
        String seeker_name = "";
        String qualification = "";
        String resume_keyword = "";
        String resume_download = "";
        String Job_details = "";
        String i_name = "";
        String j_type = "";
        String last_date_a = "";
        String job_type_select = "";
        String select_desi = "";
        String select_job_loc = "";
        String select_sp = "";
        String select_exp = "";
        String select_r_range = "";
        String enter_per_cgpa = "";
        String invelid_per_cgpa = "";
        String enter_tech = "";
        String enter_number_of_v = "";
        String enter_meta_desc = "";
        String enter_meta_key = "";
        String enter_a_name = "";
        String enter_job_desc = "";
        String enter_min_s = "";
        String enter_max_S = "";
        String min_less_max = "";
        String min_s_num = "";
        String max_s_num = "";
        String job_post_limit_out = "";
        String job_post_success = "";
        String job_not_post = "";
        String job_up_success = "";
        String job_not_up = "";
        String pricing_plans_heading = "";
        String paymenu_heading = "";
        String pay_with_key = "";
        String pay_now_btn = "";
        String profile_update_err = "";
        String pdf_doc_allow_resume = "";
        String old_ps_enter = "";
        String fresher_keyword = "";
        String experienced_keyword = "";
        String all_field_required = "";
        String delete_job_msg = "";
        String delete_keyword = "";
        String rate_this_recruiter = "";
        String write_review_fr_recruiter = "";
        String add_review = "";
        String rate_app = "";

        public String getRate_app() {
            return rate_app;
        }

        public void setRate_app(String rate_app) {
            this.rate_app = rate_app;
        }

        public String getAll_recruiter() {
            return all_recruiter;
        }

        public void setAll_recruiter(String all_recruiter) {
            this.all_recruiter = all_recruiter;
        }

        String all_recruiter = "";

        public String getWrite_review_fr_recruiter() {
            return write_review_fr_recruiter;
        }

        public void setWrite_review_fr_recruiter(String write_review_fr_recruiter) {
            this.write_review_fr_recruiter = write_review_fr_recruiter;
        }

        public String getAdd_review() {
            return add_review;
        }

        public void setAdd_review(String add_review) {
            this.add_review = add_review;
        }

        public String getRate_this_recruiter() {
            return rate_this_recruiter;
        }

        public void setRate_this_recruiter(String rate_this_recruiter) {
            this.rate_this_recruiter = rate_this_recruiter;
        }

        public String getSubmit_btn() {
            return submit_btn;
        }

        public String getRec_filter_placeholder() {
            return rec_filter_placeholder;
        }

        public void setRec_filter_placeholder(String rec_filter_placeholder) {
            this.rec_filter_placeholder = rec_filter_placeholder;
        }

        public void setSubmit_btn(String submit_btn) {
            this.submit_btn = submit_btn;
        }

        public String getDelete_keyword() {
            return delete_keyword;
        }

        public void setDelete_keyword(String delete_keyword) {
            this.delete_keyword = delete_keyword;
        }

        public String getDelete_job_msg() {
            return delete_job_msg;
        }

        public void setDelete_job_msg(String delete_job_msg) {
            this.delete_job_msg = delete_job_msg;
        }

        public String getGuest_keyword() {
            return guest_keyword;
        }

        public void setGuest_keyword(String guest_keyword) {
            this.guest_keyword = guest_keyword;
        }

        public String getFill_keyword() {
            return fill_keyword;
        }

        public void setFill_keyword(String fill_keyword) {
            this.fill_keyword = fill_keyword;
        }

        String fill_keyword = "";

        public String getAll_field_required() {
            return all_field_required;
        }

        public void setAll_field_required(String all_field_required) {
            this.all_field_required = all_field_required;
        }

        public String getFresher_keyword() {
            return fresher_keyword;
        }

        public void setFresher_keyword(String fresher_keyword) {
            this.fresher_keyword = fresher_keyword;
        }

        public String getExperienced_keyword() {
            return experienced_keyword;
        }

        public void setExperienced_keyword(String experienced_keyword) {
            this.experienced_keyword = experienced_keyword;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMale_keyword() {
            return male_keyword;
        }

        public void setMale_keyword(String male_keyword) {
            this.male_keyword = male_keyword;
        }

        public String getFemale_keyword() {
            return female_keyword;
        }

        public void setFemale_keyword(String female_keyword) {
            this.female_keyword = female_keyword;
        }

        public String getOld_ps_enter() {
            return old_ps_enter;
        }

        public void setOld_ps_enter(String old_ps_enter) {
            this.old_ps_enter = old_ps_enter;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getJobs() {
            return jobs;
        }

        public void setJobs(String jobs) {
            this.jobs = jobs;
        }

        public String getApplied_job() {
            return applied_job;
        }

        public void setApplied_job(String applied_job) {
            this.applied_job = applied_job;
        }

        public String getProfile_setting() {
            return profile_setting;
        }

        public void setProfile_setting(String profile_setting) {
            this.profile_setting = profile_setting;
        }

        public String getLogout() {
            return logout;
        }

        public void setLogout(String logout) {
            this.logout = logout;
        }

        public String getHome_page_heading() {
            return home_page_heading;
        }

        public void setHome_page_heading(String home_page_heading) {
            this.home_page_heading = home_page_heading;
        }

        public String getText_filter_placeholder() {
            return text_filter_placeholder;
        }

        public void setText_filter_placeholder(String text_filter_placeholder) {
            this.text_filter_placeholder = text_filter_placeholder;
        }

        public String getFilter_up_side_msg() {
            return filter_up_side_msg;
        }

        public void setFilter_up_side_msg(String filter_up_side_msg) {
            this.filter_up_side_msg = filter_up_side_msg;
        }

        public String getView_btn() {
            return view_btn;
        }

        public void setView_btn(String view_btn) {
            this.view_btn = view_btn;
        }

        public String getApplied_keyword() {
            return applied_keyword;
        }

        public void setApplied_keyword(String applied_keyword) {
            this.applied_keyword = applied_keyword;
        }

        public String getLeft_job_type() {
            return left_job_type;
        }

        public void setLeft_job_type(String left_job_type) {
            this.left_job_type = left_job_type;
        }

        public String getLeft_location() {
            return left_location;
        }

        public void setLeft_location(String left_location) {
            this.left_location = left_location;
        }

        public String getLeft_designation() {
            return left_designation;
        }

        public void setLeft_designation(String left_designation) {
            this.left_designation = left_designation;
        }

        public String getLeft_qualification() {
            return left_qualification;
        }

        public void setLeft_qualification(String left_qualification) {
            this.left_qualification = left_qualification;
        }

        public String getLeft_experience() {
            return left_experience;
        }

        public void setLeft_experience(String left_experience) {
            this.left_experience = left_experience;
        }

        public String getSearch_locatiion() {
            return search_locatiion;
        }

        public void setSearch_locatiion(String search_locatiion) {
            this.search_locatiion = search_locatiion;
        }

        public String getMy_applied_job_headding() {
            return my_applied_job_headding;
        }

        public void setMy_applied_job_headding(String my_applied_job_headding) {
            this.my_applied_job_headding = my_applied_job_headding;
        }

        public String getU_profile_keyword() {
            return u_profile_keyword;
        }

        public void setU_profile_keyword(String u_profile_keyword) {
            this.u_profile_keyword = u_profile_keyword;
        }

        public String getUser_personal_details() {
            return user_personal_details;
        }

        public void setUser_personal_details(String user_personal_details) {
            this.user_personal_details = user_personal_details;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_mno() {
            return user_mno;
        }

        public void setUser_mno(String user_mno) {
            this.user_mno = user_mno;
        }

        public String getUser_address() {
            return user_address;
        }

        public void setUser_address(String user_address) {
            this.user_address = user_address;
        }

        public String getUser_search_preferences() {
            return user_search_preferences;
        }

        public void setUser_search_preferences(String user_search_preferences) {
            this.user_search_preferences = user_search_preferences;
        }

        public String getUser_location() {
            return user_location;
        }

        public void setUser_location(String user_location) {
            this.user_location = user_location;
        }

        public String getUser_job_type() {
            return user_job_type;
        }

        public void setUser_job_type(String user_job_type) {
            this.user_job_type = user_job_type;
        }

        public String getUser_qualification() {
            return user_qualification;
        }

        public void setUser_qualification(String user_qualification) {
            this.user_qualification = user_qualification;
        }

        public String getUser_passing_year() {
            return user_passing_year;
        }

        public void setUser_passing_year(String user_passing_year) {
            this.user_passing_year = user_passing_year;
        }

        public String getUser_cgpa() {
            return user_cgpa;
        }

        public void setUser_cgpa(String user_cgpa) {
            this.user_cgpa = user_cgpa;
        }

        public String getUser_aofs() {
            return user_aofs;
        }

        public void setUser_aofs(String user_aofs) {
            this.user_aofs = user_aofs;
        }

        public String getUser_exp() {
            return user_exp;
        }

        public void setUser_exp(String user_exp) {
            this.user_exp = user_exp;
        }

        public String getUser_resume() {
            return user_resume;
        }

        public void setUser_resume(String user_resume) {
            this.user_resume = user_resume;
        }

        public String getUser_resume_download() {
            return user_resume_download;
        }

        public void setUser_resume_download(String user_resume_download) {
            this.user_resume_download = user_resume_download;
        }

        public String getUser_password_change() {
            return user_password_change;
        }

        public void setUser_password_change(String user_password_change) {
            this.user_password_change = user_password_change;
        }

        public String getUser_ps() {
            return user_ps;
        }

        public void setUser_ps(String user_ps) {
            this.user_ps = user_ps;
        }

        public String getUser_cps() {
            return user_cps;
        }

        public void setUser_cps(String user_cps) {
            this.user_cps = user_cps;
        }

        public String getU_p_save_btn() {
            return u_p_save_btn;
        }

        public void setU_p_save_btn(String u_p_save_btn) {
            this.u_p_save_btn = u_p_save_btn;
        }

        public String getLogin_heading() {
            return login_heading;
        }

        public void setLogin_heading(String login_heading) {
            this.login_heading = login_heading;
        }

        public String getLogin_option1() {
            return login_option1;
        }

        public void setLogin_option1(String login_option1) {
            this.login_option1 = login_option1;
        }

        public String getLogin_option2() {
            return login_option2;
        }

        public void setLogin_option2(String login_option2) {
            this.login_option2 = login_option2;
        }

        public String getEmail_login() {
            return email_login;
        }

        public void setEmail_login(String email_login) {
            this.email_login = email_login;
        }

        public String getPassword_login() {
            return password_login;
        }

        public void setPassword_login(String password_login) {
            this.password_login = password_login;
        }

        public String getForgot_password() {
            return forgot_password;
        }

        public void setForgot_password(String forgot_password) {
            this.forgot_password = forgot_password;
        }

        public String getLogin_btn() {
            return login_btn;
        }

        public void setLogin_btn(String login_btn) {
            this.login_btn = login_btn;
        }

        public String getRegister_msg() {
            return register_msg;
        }

        public void setRegister_msg(String register_msg) {
            this.register_msg = register_msg;
        }

        public String getRegister_keyword() {
            return Register_keyword;
        }

        public void setRegister_keyword(String register_keyword) {
            Register_keyword = register_keyword;
        }

        public String getForgot_ps_heading() {
            return forgot_ps_heading;
        }

        public void setForgot_ps_heading(String forgot_ps_heading) {
            this.forgot_ps_heading = forgot_ps_heading;
        }

        public String getForgot_ps_option1() {
            return forgot_ps_option1;
        }

        public void setForgot_ps_option1(String forgot_ps_option1) {
            this.forgot_ps_option1 = forgot_ps_option1;
        }

        public String getForgot_ps_option2() {
            return forgot_ps_option2;
        }

        public void setForgot_ps_option2(String forgot_ps_option2) {
            this.forgot_ps_option2 = forgot_ps_option2;
        }

        public String getForget_ps_email() {
            return forget_ps_email;
        }

        public void setForget_ps_email(String forget_ps_email) {
            this.forget_ps_email = forget_ps_email;
        }

        public String getForgot_ps_btn() {
            return forgot_ps_btn;
        }

        public void setForgot_ps_btn(String forgot_ps_btn) {
            this.forgot_ps_btn = forgot_ps_btn;
        }

        public String getRegister_page_heading() {
            return register_page_heading;
        }

        public void setRegister_page_heading(String register_page_heading) {
            this.register_page_heading = register_page_heading;
        }

        public String getRegister_email() {
            return register_email;
        }

        public void setRegister_email(String register_email) {
            this.register_email = register_email;
        }

        public String getResister_mno() {
            return resister_mno;
        }

        public void setResister_mno(String resister_mno) {
            this.resister_mno = resister_mno;
        }

        public String getRegister_ps() {
            return register_ps;
        }

        public void setRegister_ps(String register_ps) {
            this.register_ps = register_ps;
        }

        public String getRegister_cps() {
            return register_cps;
        }

        public void setRegister_cps(String register_cps) {
            this.register_cps = register_cps;
        }

        public String getRegister_btn() {
            return register_btn;
        }

        public void setRegister_btn(String register_btn) {
            this.register_btn = register_btn;
        }

        public String getLogin_msg() {
            return login_msg;
        }

        public void setLogin_msg(String login_msg) {
            this.login_msg = login_msg;
        }

        public String getLogin_keyword() {
            return login_keyword;
        }

        public void setLogin_keyword(String login_keyword) {
            this.login_keyword = login_keyword;
        }

        public String getRegister_option1() {
            return register_option1;
        }

        public void setRegister_option1(String register_option1) {
            this.register_option1 = register_option1;
        }

        public String getRegister_option2() {
            return register_option2;
        }

        public void setRegister_option2(String register_option2) {
            this.register_option2 = register_option2;
        }

        public String getRegister_name() {
            return register_name;
        }

        public void setRegister_name(String register_name) {
            this.register_name = register_name;
        }

        public String getMy_jobs1() {
            return my_jobs1;
        }

        public void setMy_jobs1(String my_jobs1) {
            this.my_jobs1 = my_jobs1;
        }

        public String getPost_a_job1() {
            return post_a_job1;
        }

        public void setPost_a_job1(String post_a_job1) {
            this.post_a_job1 = post_a_job1;
        }

        public String getR_profile_setting() {
            return r_profile_setting;
        }

        public void setR_profile_setting(String r_profile_setting) {
            this.r_profile_setting = r_profile_setting;
        }

        public String getApplied_seeker() {
            return applied_seeker;
        }

        public void setApplied_seeker(String applied_seeker) {
            this.applied_seeker = applied_seeker;
        }

        public String getRecruiter_home_heading() {
            return recruiter_home_heading;
        }

        public void setRecruiter_home_heading(String recruiter_home_heading) {
            this.recruiter_home_heading = recruiter_home_heading;
        }

        public String getEdit_keyword() {
            return edit_keyword;
        }

        public void setEdit_keyword(String edit_keyword) {
            this.edit_keyword = edit_keyword;
        }

        public String getChange_logo() {
            return change_logo;
        }

        public void setChange_logo(String change_logo) {
            this.change_logo = change_logo;
        }

        public String getOrg_type_title() {
            return org_type_title;
        }

        public void setOrg_type_title(String org_type_title) {
            this.org_type_title = org_type_title;
        }

        public String getType1() {
            return type1;
        }

        public void setType1(String type1) {
            this.type1 = type1;
        }

        public String getType2() {
            return type2;
        }

        public void setType2(String type2) {
            this.type2 = type2;
        }

        public String getOrg_name() {
            return org_name;
        }

        public void setOrg_name(String org_name) {
            this.org_name = org_name;
        }

        public String getOrg_location11() {
            return org_location11;
        }

        public void setOrg_location11(String org_location11) {
            this.org_location11 = org_location11;
        }

        public String getOrg_address1() {
            return org_address1;
        }

        public void setOrg_address1(String org_address1) {
            this.org_address1 = org_address1;
        }

        public String getOrg_website() {
            return org_website;
        }

        public void setOrg_website(String org_website) {
            this.org_website = org_website;
        }

        public String getOrg_desc() {
            return org_desc;
        }

        public void setOrg_desc(String org_desc) {
            this.org_desc = org_desc;
        }

        public String getPay_detail() {
            return pay_detail;
        }

        public void setPay_detail(String pay_detail) {
            this.pay_detail = pay_detail;
        }

        public String getPay_date() {
            return pay_date;
        }

        public void setPay_date(String pay_date) {
            this.pay_date = pay_date;
        }

        public String getPlan() {
            return plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getJob_post() {
            return job_post;
        }

        public void setJob_post(String job_post) {
            this.job_post = job_post;
        }

        public String getR_download() {
            return r_download;
        }

        public void setR_download(String r_download) {
            this.r_download = r_download;
        }

        public String getValidity() {
            return validity;
        }

        public void setValidity(String validity) {
            this.validity = validity;
        }

        public String getActive_new_plan() {
            return active_new_plan;
        }

        public void setActive_new_plan(String active_new_plan) {
            this.active_new_plan = active_new_plan;
        }

        public String getJob_post_heading1() {
            return job_post_heading1;
        }

        public void setJob_post_heading1(String job_post_heading1) {
            this.job_post_heading1 = job_post_heading1;
        }

        public String getJob_type() {
            return job_type;
        }

        public void setJob_type(String job_type) {
            this.job_type = job_type;
        }

        public String getDesi() {
            return desi;
        }

        public void setDesi(String desi) {
            this.desi = desi;
        }

        public String getQua() {
            return qua;
        }

        public void setQua(String qua) {
            this.qua = qua;
        }

        public String getJob_location() {
            return job_location;
        }

        public void setJob_location(String job_location) {
            this.job_location = job_location;
        }

        public String getP_year() {
            return p_year;
        }

        public void setP_year(String p_year) {
            this.p_year = p_year;
        }

        public String getJob_cgpa() {
            return job_cgpa;
        }

        public void setJob_cgpa(String job_cgpa) {
            this.job_cgpa = job_cgpa;
        }

        public String getSp() {
            return sp;
        }

        public void setSp(String sp) {
            this.sp = sp;
        }

        public String getJob_aofs() {
            return job_aofs;
        }

        public void setJob_aofs(String job_aofs) {
            this.job_aofs = job_aofs;
        }

        public String getJob_exp() {
            return job_exp;
        }

        public void setJob_exp(String job_exp) {
            this.job_exp = job_exp;
        }

        public String getNum_if_v() {
            return num_if_v;
        }

        public void setNum_if_v(String num_if_v) {
            this.num_if_v = num_if_v;
        }

        public String getLast_date_of_a() {
            return last_date_of_a;
        }

        public void setLast_date_of_a(String last_date_of_a) {
            this.last_date_of_a = last_date_of_a;
        }

        public String getTech() {
            return tech;
        }

        public void setTech(String tech) {
            this.tech = tech;
        }

        public String getH_pro() {
            return h_pro;
        }

        public void setH_pro(String h_pro) {
            this.h_pro = h_pro;
        }

        public String getJob_desc1() {
            return job_desc1;
        }

        public void setJob_desc1(String job_desc1) {
            this.job_desc1 = job_desc1;
        }

        public String getS_range() {
            return s_range;
        }

        public void setS_range(String s_range) {
            this.s_range = s_range;
        }

        public String getMin1() {
            return min1;
        }

        public void setMin1(String min1) {
            this.min1 = min1;
        }

        public String getMax1() {
            return max1;
        }

        public void setMax1(String max1) {
            this.max1 = max1;
        }

        public String getMeta_desc1() {
            return meta_desc1;
        }

        public void setMeta_desc1(String meta_desc1) {
            this.meta_desc1 = meta_desc1;
        }

        public String getMeta_keyword1() {
            return meta_keyword1;
        }

        public void setMeta_keyword1(String meta_keyword1) {
            this.meta_keyword1 = meta_keyword1;
        }

        public String getAname() {
            return aname;
        }

        public void setAname(String aname) {
            this.aname = aname;
        }

        public String getJob_post_btn() {
            return job_post_btn;
        }

        public void setJob_post_btn(String job_post_btn) {
            this.job_post_btn = job_post_btn;
        }

        public String getSelect_keyword() {
            return select_keyword;
        }

        public void setSelect_keyword(String select_keyword) {
            this.select_keyword = select_keyword;
        }

        public String getJon_tool_top1() {
            return jon_tool_top1;
        }

        public void setJon_tool_top1(String jon_tool_top1) {
            this.jon_tool_top1 = jon_tool_top1;
        }

        public String getJon_tool_top2() {
            return jon_tool_top2;
        }

        public void setJon_tool_top2(String jon_tool_top2) {
            this.jon_tool_top2 = jon_tool_top2;
        }

        public String getS_written_test_k() {
            return s_written_test_k;
        }

        public void setS_written_test_k(String s_written_test_k) {
            this.s_written_test_k = s_written_test_k;
        }

        public String getS_gtoup_d_k() {
            return s_gtoup_d_k;
        }

        public void setS_gtoup_d_k(String s_gtoup_d_k) {
            this.s_gtoup_d_k = s_gtoup_d_k;
        }

        public String getS_r_round_k() {
            return s_r_round_k;
        }

        public void setS_r_round_k(String s_r_round_k) {
            this.s_r_round_k = s_r_round_k;
        }

        public String getS_hr_round_k() {
            return s_hr_round_k;
        }

        public void setS_hr_round_k(String s_hr_round_k) {
            this.s_hr_round_k = s_hr_round_k;
        }

        public String getPer_year1() {
            return per_year1;
        }

        public void setPer_year1(String per_year1) {
            this.per_year1 = per_year1;
        }

        public String getPer_month() {
            return per_month;
        }

        public void setPer_month(String per_month) {
            this.per_month = per_month;
        }

        public String getPer_day() {
            return per_day;
        }

        public void setPer_day(String per_day) {
            this.per_day = per_day;
        }

        public String getPer_hour() {
            return per_hour;
        }

        public void setPer_hour(String per_hour) {
            this.per_hour = per_hour;
        }

        public String getEdit_job_post_heading() {
            return edit_job_post_heading;
        }

        public void setEdit_job_post_heading(String edit_job_post_heading) {
            this.edit_job_post_heading = edit_job_post_heading;
        }

        public String getEdit_job_btn() {
            return edit_job_btn;
        }

        public void setEdit_job_btn(String edit_job_btn) {
            this.edit_job_btn = edit_job_btn;
        }

        public String getSingle_form_keyword() {
            return single_form_keyword;
        }

        public void setSingle_form_keyword(String single_form_keyword) {
            this.single_form_keyword = single_form_keyword;
        }

        public String getSingle_organisation_keyword() {
            return single_organisation_keyword;
        }

        public void setSingle_organisation_keyword(String single_organisation_keyword) {
            this.single_organisation_keyword = single_organisation_keyword;
        }

        public String getSingle_job_post_date() {
            return single_job_post_date;
        }

        public void setSingle_job_post_date(String single_job_post_date) {
            this.single_job_post_date = single_job_post_date;
        }

        public String getSingle_job_type() {
            return single_job_type;
        }

        public void setSingle_job_type(String single_job_type) {
            this.single_job_type = single_job_type;
        }

        public String getSingle_designation() {
            return single_designation;
        }

        public void setSingle_designation(String single_designation) {
            this.single_designation = single_designation;
        }

        public String getSingle_qualification() {
            return single_qualification;
        }

        public void setSingle_qualification(String single_qualification) {
            this.single_qualification = single_qualification;
        }

        public String getSingle_specialization() {
            return single_specialization;
        }

        public void setSingle_specialization(String single_specialization) {
            this.single_specialization = single_specialization;
        }

        public String getSingle_application_last_date() {
            return single_application_last_date;
        }

        public void setSingle_application_last_date(String single_application_last_date) {
            this.single_application_last_date = single_application_last_date;
        }

        public String getSingle_min() {
            return single_min;
        }

        public void setSingle_min(String single_min) {
            this.single_min = single_min;
        }

        public String getSingle_max() {
            return single_max;
        }

        public void setSingle_max(String single_max) {
            this.single_max = single_max;
        }

        public String getSingle_h_pro() {
            return single_h_pro;
        }

        public void setSingle_h_pro(String single_h_pro) {
            this.single_h_pro = single_h_pro;
        }

        public String getSingle_job_desc() {
            return single_job_desc;
        }

        public void setSingle_job_desc(String single_job_desc) {
            this.single_job_desc = single_job_desc;
        }

        public String getSingle_org_d() {
            return single_org_d;
        }

        public void setSingle_org_d(String single_org_d) {
            this.single_org_d = single_org_d;
        }

        public String getSingle_type() {
            return single_type;
        }

        public void setSingle_type(String single_type) {
            this.single_type = single_type;
        }

        public String getSingle_location() {
            return single_location;
        }

        public void setSingle_location(String single_location) {
            this.single_location = single_location;
        }

        public String getSingle_website() {
            return single_website;
        }

        public void setSingle_website(String single_website) {
            this.single_website = single_website;
        }

        public String getApplay_btn() {
            return applay_btn;
        }

        public void setApplay_btn(String applay_btn) {
            this.applay_btn = applay_btn;
        }

        public String getAlready_applied_btn() {
            return already_applied_btn;
        }

        public void setAlready_applied_btn(String already_applied_btn) {
            this.already_applied_btn = already_applied_btn;
        }

        public String getL_back_to_home() {
            return l_back_to_home;
        }

        public void setL_back_to_home(String l_back_to_home) {
            this.l_back_to_home = l_back_to_home;
        }

        public String getL_or_keyword() {
            return l_or_keyword;
        }

        public void setL_or_keyword(String l_or_keyword) {
            this.l_or_keyword = l_or_keyword;
        }

        public String getValidaton_email_empty() {
            return validaton_email_empty;
        }

        public void setValidaton_email_empty(String validaton_email_empty) {
            this.validaton_email_empty = validaton_email_empty;
        }

        public String getValidaton_password_empty() {
            return validaton_password_empty;
        }

        public void setValidaton_password_empty(String validaton_password_empty) {
            this.validaton_password_empty = validaton_password_empty;
        }

        public String getValidation_valid_email() {
            return validation_valid_email;
        }

        public void setValidation_valid_email(String validation_valid_email) {
            this.validation_valid_email = validation_valid_email;
        }

        public String getValidation_password_short() {
            return validation_password_short;
        }

        public void setValidation_password_short(String validation_password_short) {
            this.validation_password_short = validation_password_short;
        }

        public String getValidation_email_veri() {
            return validation_email_veri;
        }

        public void setValidation_email_veri(String validation_email_veri) {
            this.validation_email_veri = validation_email_veri;
        }

        public String getValidation_account_dis() {
            return validation_account_dis;
        }

        public void setValidation_account_dis(String validation_account_dis) {
            this.validation_account_dis = validation_account_dis;
        }

        public String getValidation_login_error() {
            return validation_login_error;
        }

        public void setValidation_login_error(String validation_login_error) {
            this.validation_login_error = validation_login_error;
        }

        public String getValidation_name_empty() {
            return validation_name_empty;
        }

        public void setValidation_name_empty(String validation_name_empty) {
            this.validation_name_empty = validation_name_empty;
        }

        public String getValidation_mno_empty() {
            return validation_mno_empty;
        }

        public void setValidation_mno_empty(String validation_mno_empty) {
            this.validation_mno_empty = validation_mno_empty;
        }

        public String getValidation_cps_empty() {
            return validation_cps_empty;
        }

        public void setValidation_cps_empty(String validation_cps_empty) {
            this.validation_cps_empty = validation_cps_empty;
        }

        public String getValidation_valid_mno() {
            return validation_valid_mno;
        }

        public void setValidation_valid_mno(String validation_valid_mno) {
            this.validation_valid_mno = validation_valid_mno;
        }

        public String getValidation_email_exists() {
            return validation_email_exists;
        }

        public void setValidation_email_exists(String validation_email_exists) {
            this.validation_email_exists = validation_email_exists;
        }

        public String getValidation_mno_exists() {
            return validation_mno_exists;
        }

        public void setValidation_mno_exists(String validation_mno_exists) {
            this.validation_mno_exists = validation_mno_exists;
        }

        public String getValidation_new_ps_send() {
            return validation_new_ps_send;
        }

        public void setValidation_new_ps_send(String validation_new_ps_send) {
            this.validation_new_ps_send = validation_new_ps_send;
        }

        public String getValidation_select_plocation() {
            return validation_select_plocation;
        }

        public void setValidation_select_plocation(String validation_select_plocation) {
            this.validation_select_plocation = validation_select_plocation;
        }

        public String getValidation() {
            return validation;
        }

        public void setValidation(String validation) {
            this.validation = validation;
        }

        public String getValidation_select_qua() {
            return validation_select_qua;
        }

        public void setValidation_select_qua(String validation_select_qua) {
            this.validation_select_qua = validation_select_qua;
        }

        public String getValidation_cps_short() {
            return validation_cps_short;
        }

        public void setValidation_cps_short(String validation_cps_short) {
            this.validation_cps_short = validation_cps_short;
        }

        public String getValidation_ps_not() {
            return validation_ps_not;
        }

        public void setValidation_ps_not(String validation_ps_not) {
            this.validation_ps_not = validation_ps_not;
        }

        public String getValidation_not_found_msg() {
            return validation_not_found_msg;
        }

        public void setValidation_not_found_msg(String validation_not_found_msg) {
            this.validation_not_found_msg = validation_not_found_msg;
        }

        public String getValidation_not_found_msg2() {
            return validation_not_found_msg2;
        }

        public void setValidation_not_found_msg2(String validation_not_found_msg2) {
            this.validation_not_found_msg2 = validation_not_found_msg2;
        }

        public String getValidation_select_aofs() {
            return validation_select_aofs;
        }

        public void setValidation_select_aofs(String validation_select_aofs) {
            this.validation_select_aofs = validation_select_aofs;
        }

        public String getValidation_enter_address() {
            return validation_enter_address;
        }

        public void setValidation_enter_address(String validation_enter_address) {
            this.validation_enter_address = validation_enter_address;
        }

        public String getValidation_pyeat() {
            return validation_pyeat;
        }

        public void setValidation_pyeat(String validation_pyeat) {
            this.validation_pyeat = validation_pyeat;
        }

        public String getValidation_enter_cgpa() {
            return validation_enter_cgpa;
        }

        public void setValidation_enter_cgpa(String validation_enter_cgpa) {
            this.validation_enter_cgpa = validation_enter_cgpa;
        }

        public String getValidation_v_cgpa() {
            return validation_v_cgpa;
        }

        public void setValidation_v_cgpa(String validation_v_cgpa) {
            this.validation_v_cgpa = validation_v_cgpa;
        }

        public String getValidation_select_resume() {
            return validation_select_resume;
        }

        public void setValidation_select_resume(String validation_select_resume) {
            this.validation_select_resume = validation_select_resume;
        }

        public String getValidation_profile_success() {
            return validation_profile_success;
        }

        public void setValidation_profile_success(String validation_profile_success) {
            this.validation_profile_success = validation_profile_success;
        }

        public String getValidation_org_name() {
            return validation_org_name;
        }

        public void setValidation_org_name(String validation_org_name) {
            this.validation_org_name = validation_org_name;
        }

        public String getValidation_org_loc() {
            return validation_org_loc;
        }

        public void setValidation_org_loc(String validation_org_loc) {
            this.validation_org_loc = validation_org_loc;
        }

        public String getValidation_org_address() {
            return validation_org_address;
        }

        public void setValidation_org_address(String validation_org_address) {
            this.validation_org_address = validation_org_address;
        }

        public String getValidation_website_link() {
            return validation_website_link;
        }

        public void setValidation_website_link(String validation_website_link) {
            this.validation_website_link = validation_website_link;
        }

        public String getValidation_org_desc() {
            return validation_org_desc;
        }

        public void setValidation_org_desc(String validation_org_desc) {
            this.validation_org_desc = validation_org_desc;
        }

        public String getValidation_valid_link() {
            return validation_valid_link;
        }

        public void setValidation_valid_link(String validation_valid_link) {
            this.validation_valid_link = validation_valid_link;
        }

        public String getHome_not_avil_msg() {
            return home_not_avil_msg;
        }

        public void setHome_not_avil_msg(String home_not_avil_msg) {
            this.home_not_avil_msg = home_not_avil_msg;
        }

        public String getH_job_not_found() {
            return h_job_not_found;
        }

        public void setH_job_not_found(String h_job_not_found) {
            this.h_job_not_found = h_job_not_found;
        }

        public String getH_work_exp() {
            return h_work_exp;
        }

        public void setH_work_exp(String h_work_exp) {
            this.h_work_exp = h_work_exp;
        }

        public String getH_job_location() {
            return h_job_location;
        }

        public void setH_job_location(String h_job_location) {
            this.h_job_location = h_job_location;
        }

        public String getH_req_skils() {
            return h_req_skils;
        }

        public void setH_req_skils(String h_req_skils) {
            this.h_req_skils = h_req_skils;
        }

        public String getH_icon() {
            return h_icon;
        }

        public void setH_icon(String h_icon) {
            this.h_icon = h_icon;
        }

        public String getH_skils() {
            return h_skils;
        }

        public void setH_skils(String h_skils) {
            this.h_skils = h_skils;
        }

        public String getR_back_to_home() {
            return r_back_to_home;
        }

        public void setR_back_to_home(String r_back_to_home) {
            this.r_back_to_home = r_back_to_home;
        }

        public String getR_or() {
            return r_or;
        }

        public void setR_or(String r_or) {
            this.r_or = r_or;
        }

        public String getMy_a_applied() {
            return my_a_applied;
        }

        public void setMy_a_applied(String my_a_applied) {
            this.my_a_applied = my_a_applied;
        }

        public String getMy_a_view() {
            return my_a_view;
        }

        public void setMy_a_view(String my_a_view) {
            this.my_a_view = my_a_view;
        }

        public String getS_tool_tip1() {
            return s_tool_tip1;
        }

        public void setS_tool_tip1(String s_tool_tip1) {
            this.s_tool_tip1 = s_tool_tip1;
        }

        public String getS_tool_tip2() {
            return s_tool_tip2;
        }

        public void setS_tool_tip2(String s_tool_tip2) {
            this.s_tool_tip2 = s_tool_tip2;
        }

        public String getS_tool_tip3() {
            return s_tool_tip3;
        }

        public void setS_tool_tip3(String s_tool_tip3) {
            this.s_tool_tip3 = s_tool_tip3;
        }

        public String getUser_p_select() {
            return user_p_select;
        }

        public void setUser_p_select(String user_p_select) {
            this.user_p_select = user_p_select;
        }

        public String getR_logout() {
            return r_logout;
        }

        public void setR_logout(String r_logout) {
            this.r_logout = r_logout;
        }

        public String getR_applied() {
            return r_applied;
        }

        public void setR_applied(String r_applied) {
            this.r_applied = r_applied;
        }

        public String getR_view() {
            return r_view;
        }

        public void setR_view(String r_view) {
            this.r_view = r_view;
        }

        public String getR_edit() {
            return r_edit;
        }

        public void setR_edit(String r_edit) {
            this.r_edit = r_edit;
        }

        public String getR_tool_tip1() {
            return r_tool_tip1;
        }

        public void setR_tool_tip1(String r_tool_tip1) {
            this.r_tool_tip1 = r_tool_tip1;
        }

        public String getR_tool_tip2() {
            return r_tool_tip2;
        }

        public void setR_tool_tip2(String r_tool_tip2) {
            this.r_tool_tip2 = r_tool_tip2;
        }

        public String getR_tool_tip3() {
            return r_tool_tip3;
        }

        public void setR_tool_tip3(String r_tool_tip3) {
            this.r_tool_tip3 = r_tool_tip3;
        }

        public String getR_j__written_test_k() {
            return r_j__written_test_k;
        }

        public void setR_j__written_test_k(String r_j__written_test_k) {
            this.r_j__written_test_k = r_j__written_test_k;
        }

        public String getR_j__hr_round_k() {
            return r_j__hr_round_k;
        }

        public void setR_j__hr_round_k(String r_j__hr_round_k) {
            this.r_j__hr_round_k = r_j__hr_round_k;
        }

        public String getR_j__r_round_k() {
            return r_j__r_round_k;
        }

        public void setR_j__r_round_k(String r_j__r_round_k) {
            this.r_j__r_round_k = r_j__r_round_k;
        }

        public String getR_j__gtoup_d_k() {
            return r_j__gtoup_d_k;
        }

        public void setR_j__gtoup_d_k(String r_j__gtoup_d_k) {
            this.r_j__gtoup_d_k = r_j__gtoup_d_k;
        }

        public String getJob_post_select() {
            return job_post_select;
        }

        public void setJob_post_select(String job_post_select) {
            this.job_post_select = job_post_select;
        }

        public String getJob_posted() {
            return job_posted;
        }

        public void setJob_posted(String job_posted) {
            this.job_posted = job_posted;
        }

        public String getForm_k() {
            return form_k;
        }

        public void setForm_k(String form_k) {
            this.form_k = form_k;
        }

        public String getOrg_k() {
            return org_k;
        }

        public void setOrg_k(String org_k) {
            this.org_k = org_k;
        }

        public String getOrg_type() {
            return org_type;
        }

        public void setOrg_type(String org_type) {
            this.org_type = org_type;
        }

        public String getJ_location() {
            return j_location;
        }

        public void setJ_location(String j_location) {
            this.j_location = j_location;
        }

        public String getJ_website() {
            return j_website;
        }

        public void setJ_website(String j_website) {
            this.j_website = j_website;
        }

        public String getTt1() {
            return tt1;
        }

        public void setTt1(String tt1) {
            this.tt1 = tt1;
        }

        public String getTt2() {
            return tt2;
        }

        public void setTt2(String tt2) {
            this.tt2 = tt2;
        }

        public String getTt3() {
            return tt3;
        }

        public void setTt3(String tt3) {
            this.tt3 = tt3;
        }

        public String getS_change() {
            return s_change;
        }

        public void setS_change(String s_change) {
            this.s_change = s_change;
        }

        public String getRec_pay_error() {
            return rec_pay_error;
        }

        public void setRec_pay_error(String rec_pay_error) {
            this.rec_pay_error = rec_pay_error;
        }

        public String getProfile_keyword() {
            return profile_keyword;
        }

        public void setProfile_keyword(String profile_keyword) {
            this.profile_keyword = profile_keyword;
        }

        public String getApplied_s_heading() {
            return applied_s_heading;
        }

        public void setApplied_s_heading(String applied_s_heading) {
            this.applied_s_heading = applied_s_heading;
        }

        public String getA_s_error() {
            return a_s_error;
        }

        public void setA_s_error(String a_s_error) {
            this.a_s_error = a_s_error;
        }

        public String getJob_btn() {
            return job_btn;
        }

        public void setJob_btn(String job_btn) {
            this.job_btn = job_btn;
        }

        public String getPs_not_change() {
            return ps_not_change;
        }

        public void setPs_not_change(String ps_not_change) {
            this.ps_not_change = ps_not_change;
        }

        public String getC_address() {
            return c_address;
        }

        public void setC_address(String c_address) {
            this.c_address = c_address;
        }

        public String getPro_not_up() {
            return pro_not_up;
        }

        public void setPro_not_up(String pro_not_up) {
            this.pro_not_up = pro_not_up;
        }

        public String getOnly_jpg() {
            return only_jpg;
        }

        public void setOnly_jpg(String only_jpg) {
            this.only_jpg = only_jpg;
        }

        public String getPpic_not() {
            return ppic_not;
        }

        public void setPpic_not(String ppic_not) {
            this.ppic_not = ppic_not;
        }

        public String getPro_pic_up() {
            return pro_pic_up;
        }

        public void setPro_pic_up(String pro_pic_up) {
            this.pro_pic_up = pro_pic_up;
        }

        public String getFilter_error() {
            return filter_error;
        }

        public void setFilter_error(String filter_error) {
            this.filter_error = filter_error;
        }

        public String getApplay_success() {
            return applay_success;
        }

        public void setApplay_success(String applay_success) {
            this.applay_success = applay_success;
        }

        public String getActive_new_p() {
            return active_new_p;
        }

        public void setActive_new_p(String active_new_p) {
            this.active_new_p = active_new_p;
        }

        public String getApplied_seeker_not_found() {
            return applied_seeker_not_found;
        }

        public void setApplied_seeker_not_found(String applied_seeker_not_found) {
            this.applied_seeker_not_found = applied_seeker_not_found;
        }

        public String getSeeker_d_heading() {
            return seeker_d_heading;
        }

        public void setSeeker_d_heading(String seeker_d_heading) {
            this.seeker_d_heading = seeker_d_heading;
        }

        public String getSeeker_name() {
            return seeker_name;
        }

        public void setSeeker_name(String seeker_name) {
            this.seeker_name = seeker_name;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getResume_keyword() {
            return resume_keyword;
        }

        public void setResume_keyword(String resume_keyword) {
            this.resume_keyword = resume_keyword;
        }

        public String getResume_download() {
            return resume_download;
        }

        public void setResume_download(String resume_download) {
            this.resume_download = resume_download;
        }

        public String getJob_details() {
            return Job_details;
        }

        public void setJob_details(String job_details) {
            Job_details = job_details;
        }

        public String getI_name() {
            return i_name;
        }

        public void setI_name(String i_name) {
            this.i_name = i_name;
        }

        public String getJ_type() {
            return j_type;
        }

        public void setJ_type(String j_type) {
            this.j_type = j_type;
        }

        public String getLast_date_a() {
            return last_date_a;
        }

        public void setLast_date_a(String last_date_a) {
            this.last_date_a = last_date_a;
        }

        public String getJob_type_select() {
            return job_type_select;
        }

        public void setJob_type_select(String job_type_select) {
            this.job_type_select = job_type_select;
        }

        public String getSelect_desi() {
            return select_desi;
        }

        public void setSelect_desi(String select_desi) {
            this.select_desi = select_desi;
        }

        public String getSelect_job_loc() {
            return select_job_loc;
        }

        public void setSelect_job_loc(String select_job_loc) {
            this.select_job_loc = select_job_loc;
        }

        public String getSelect_sp() {
            return select_sp;
        }

        public void setSelect_sp(String select_sp) {
            this.select_sp = select_sp;
        }

        public String getSelect_exp() {
            return select_exp;
        }

        public void setSelect_exp(String select_exp) {
            this.select_exp = select_exp;
        }

        public String getSelect_r_range() {
            return select_r_range;
        }

        public void setSelect_r_range(String select_r_range) {
            this.select_r_range = select_r_range;
        }

        public String getEnter_per_cgpa() {
            return enter_per_cgpa;
        }

        public void setEnter_per_cgpa(String enter_per_cgpa) {
            this.enter_per_cgpa = enter_per_cgpa;
        }

        public String getInvelid_per_cgpa() {
            return invelid_per_cgpa;
        }

        public void setInvelid_per_cgpa(String invelid_per_cgpa) {
            this.invelid_per_cgpa = invelid_per_cgpa;
        }

        public String getEnter_tech() {
            return enter_tech;
        }

        public void setEnter_tech(String enter_tech) {
            this.enter_tech = enter_tech;
        }

        public String getEnter_number_of_v() {
            return enter_number_of_v;
        }

        public void setEnter_number_of_v(String enter_number_of_v) {
            this.enter_number_of_v = enter_number_of_v;
        }

        public String getEnter_meta_desc() {
            return enter_meta_desc;
        }

        public void setEnter_meta_desc(String enter_meta_desc) {
            this.enter_meta_desc = enter_meta_desc;
        }

        public String getEnter_meta_key() {
            return enter_meta_key;
        }

        public void setEnter_meta_key(String enter_meta_key) {
            this.enter_meta_key = enter_meta_key;
        }

        public String getEnter_a_name() {
            return enter_a_name;
        }

        public void setEnter_a_name(String enter_a_name) {
            this.enter_a_name = enter_a_name;
        }

        public String getEnter_job_desc() {
            return enter_job_desc;
        }

        public void setEnter_job_desc(String enter_job_desc) {
            this.enter_job_desc = enter_job_desc;
        }

        public String getEnter_min_s() {
            return enter_min_s;
        }

        public void setEnter_min_s(String enter_min_s) {
            this.enter_min_s = enter_min_s;
        }

        public String getEnter_max_S() {
            return enter_max_S;
        }

        public void setEnter_max_S(String enter_max_S) {
            this.enter_max_S = enter_max_S;
        }

        public String getMin_less_max() {
            return min_less_max;
        }

        public void setMin_less_max(String min_less_max) {
            this.min_less_max = min_less_max;
        }

        public String getMin_s_num() {
            return min_s_num;
        }

        public void setMin_s_num(String min_s_num) {
            this.min_s_num = min_s_num;
        }

        public String getMax_s_num() {
            return max_s_num;
        }

        public void setMax_s_num(String max_s_num) {
            this.max_s_num = max_s_num;
        }

        public String getJob_post_limit_out() {
            return job_post_limit_out;
        }

        public void setJob_post_limit_out(String job_post_limit_out) {
            this.job_post_limit_out = job_post_limit_out;
        }

        public String getJob_post_success() {
            return job_post_success;
        }

        public void setJob_post_success(String job_post_success) {
            this.job_post_success = job_post_success;
        }

        public String getJob_not_post() {
            return job_not_post;
        }

        public void setJob_not_post(String job_not_post) {
            this.job_not_post = job_not_post;
        }

        public String getJob_up_success() {
            return job_up_success;
        }

        public void setJob_up_success(String job_up_success) {
            this.job_up_success = job_up_success;
        }

        public String getJob_not_up() {
            return job_not_up;
        }

        public void setJob_not_up(String job_not_up) {
            this.job_not_up = job_not_up;
        }

        public String getPricing_plans_heading() {
            return pricing_plans_heading;
        }

        public void setPricing_plans_heading(String pricing_plans_heading) {
            this.pricing_plans_heading = pricing_plans_heading;
        }

        public String getPaymenu_heading() {
            return paymenu_heading;
        }

        public void setPaymenu_heading(String paymenu_heading) {
            this.paymenu_heading = paymenu_heading;
        }

        public String getPay_with_key() {
            return pay_with_key;
        }

        public void setPay_with_key(String pay_with_key) {
            this.pay_with_key = pay_with_key;
        }

        public String getPay_now_btn() {
            return pay_now_btn;
        }

        public void setPay_now_btn(String pay_now_btn) {
            this.pay_now_btn = pay_now_btn;
        }

        public String getProfile_update_err() {
            return profile_update_err;
        }

        public void setProfile_update_err(String profile_update_err) {
            this.profile_update_err = profile_update_err;
        }

        public String getPdf_doc_allow_resume() {
            return pdf_doc_allow_resume;
        }

        public void setPdf_doc_allow_resume(String pdf_doc_allow_resume) {
            this.pdf_doc_allow_resume = pdf_doc_allow_resume;
        }
    }
}
