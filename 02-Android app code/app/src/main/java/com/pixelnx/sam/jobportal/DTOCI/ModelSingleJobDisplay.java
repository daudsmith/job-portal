package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;

public class ModelSingleJobDisplay implements Serializable {

    String staus = "";
    String message = "";
    Data data;
    Recruiter recruiter;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Recruiter getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    public class Data implements Serializable {
        String id = "";
        String job_type = "";
        String designation = "";
        String qualification = "";
        String job_location = "";
        String year_of_passing = "";
        String pre_cgpa = "";
        String specialization = "";
        String area_of_sector = "";
        String exp = "";
        String number_of_vacancies = "";
        String lasr_date_application = "";
        String hiring_process = "";
        String salary_range = "";
        String min = "";
        String max = "";
        String r_id = "";
        String pay_count = "";
        String post_date = "";
        String application = "";
        String technology = "";
        String job_desc = "";
        String written_test = "";
        String group_discussion = "";
        String technical_round = "";
        String hr_round = "";
        String meta_desc = "";
        String meta_keyword = "";
        String author = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getJob_type() {
            return job_type;
        }

        public void setJob_type(String job_type) {
            this.job_type = job_type;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getJob_location() {
            return job_location;
        }

        public void setJob_location(String job_location) {
            this.job_location = job_location;
        }

        public String getYear_of_passing() {
            return year_of_passing;
        }

        public void setYear_of_passing(String year_of_passing) {
            this.year_of_passing = year_of_passing;
        }

        public String getPre_cgpa() {
            return pre_cgpa;
        }

        public void setPre_cgpa(String pre_cgpa) {
            this.pre_cgpa = pre_cgpa;
        }

        public String getSpecialization() {
            return specialization;
        }

        public void setSpecialization(String specialization) {
            this.specialization = specialization;
        }

        public String getArea_of_sector() {
            return area_of_sector;
        }

        public void setArea_of_sector(String area_of_sector) {
            this.area_of_sector = area_of_sector;
        }

        public String getExp() {
            return exp;
        }

        public void setExp(String exp) {
            this.exp = exp;
        }

        public String getNumber_of_vacancies() {
            return number_of_vacancies;
        }

        public void setNumber_of_vacancies(String number_of_vacancies) {
            this.number_of_vacancies = number_of_vacancies;
        }

        public String getLasr_date_application() {
            return lasr_date_application;
        }

        public void setLasr_date_application(String lasr_date_application) {
            this.lasr_date_application = lasr_date_application;
        }

        public String getHiring_process() {
            return hiring_process;
        }

        public void setHiring_process(String hiring_process) {
            this.hiring_process = hiring_process;
        }

        public String getSalary_range() {
            return salary_range;
        }

        public void setSalary_range(String salary_range) {
            this.salary_range = salary_range;
        }

        public String getMin() {
            return min;
        }

        public void setMin(String min) {
            this.min = min;
        }

        public String getMax() {
            return max;
        }

        public void setMax(String max) {
            this.max = max;
        }

        public String getR_id() {
            return r_id;
        }

        public void setR_id(String r_id) {
            this.r_id = r_id;
        }

        public String getPay_count() {
            return pay_count;
        }

        public void setPay_count(String pay_count) {
            this.pay_count = pay_count;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getApplication() {
            return application;
        }

        public void setApplication(String application) {
            this.application = application;
        }

        public String getTechnology() {
            return technology;
        }

        public void setTechnology(String technology) {
            this.technology = technology;
        }

        public String getJob_desc() {
            return job_desc;
        }

        public void setJob_desc(String job_desc) {
            this.job_desc = job_desc;
        }

        public String getWritten_test() {
            return written_test;
        }

        public void setWritten_test(String written_test) {
            this.written_test = written_test;
        }

        public String getGroup_discussion() {
            return group_discussion;
        }

        public void setGroup_discussion(String group_discussion) {
            this.group_discussion = group_discussion;
        }

        public String getTechnical_round() {
            return technical_round;
        }

        public void setTechnical_round(String technical_round) {
            this.technical_round = technical_round;
        }

        public String getHr_round() {
            return hr_round;
        }

        public void setHr_round(String hr_round) {
            this.hr_round = hr_round;
        }

        public String getMeta_desc() {
            return meta_desc;
        }

        public void setMeta_desc(String meta_desc) {
            this.meta_desc = meta_desc;
        }

        public String getMeta_keyword() {
            return meta_keyword;
        }

        public void setMeta_keyword(String meta_keyword) {
            this.meta_keyword = meta_keyword;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }
    }

    public class Recruiter implements Serializable {
        String id = "";
        String name = "";
        String email = "";
        String mno = "";
        String ps = "";
        String org_type = "";
        String location = "";
        String address = "";
        String website = "";
        String des = "";
        String veri = "";
        String img = "";
        String plan = "";
        String pay = "";
        String pay_date = "";
        String type = "";
        String month = "";
        String show_on_reg = "";
        String status = "";
        String pay_count = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMno() {
            return mno;
        }

        public void setMno(String mno) {
            this.mno = mno;
        }

        public String getPs() {
            return ps;
        }

        public void setPs(String ps) {
            this.ps = ps;
        }

        public String getOrg_type() {
            return org_type;
        }

        public void setOrg_type(String org_type) {
            this.org_type = org_type;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }

        public String getVeri() {
            return veri;
        }

        public void setVeri(String veri) {
            this.veri = veri;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getPlan() {
            return plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getPay() {
            return pay;
        }

        public void setPay(String pay) {
            this.pay = pay;
        }

        public String getPay_date() {
            return pay_date;
        }

        public void setPay_date(String pay_date) {
            this.pay_date = pay_date;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getShow_on_reg() {
            return show_on_reg;
        }

        public void setShow_on_reg(String show_on_reg) {
            this.show_on_reg = show_on_reg;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPay_count() {
            return pay_count;
        }

        public void setPay_count(String pay_count) {
            this.pay_count = pay_count;
        }
    }
}
