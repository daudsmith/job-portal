package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelPlanDTO  implements Serializable{
 String   staus="";
    String       message="";

ArrayList<Data> data;
    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data {
       String   id="";
       String   name="";
       String   amount_inr="";
       String   duration="";
       String   description="";
       String    amount_usd="";
       String    condation="";
       String    value1="";
       String    value2="";
       String   status="";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount_inr() {
            return amount_inr;
        }

        public void setAmount_inr(String amount_inr) {
            this.amount_inr = amount_inr;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAmount_usd() {
            return amount_usd;
        }

        public void setAmount_usd(String amount_usd) {
            this.amount_usd = amount_usd;
        }

        public String getCondation() {
            return condation;
        }

        public void setCondation(String condation) {
            this.condation = condation;
        }

        public String getValue1() {
            return value1;
        }

        public void setValue1(String value1) {
            this.value1 = value1;
        }

        public String getValue2() {
            return value2;
        }

        public void setValue2(String value2) {
            this.value2 = value2;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
  
}
