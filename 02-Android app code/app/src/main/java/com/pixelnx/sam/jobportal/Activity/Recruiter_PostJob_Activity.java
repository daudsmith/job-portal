package com.pixelnx.sam.jobportal.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.DTO.CommonDTO;
import com.pixelnx.sam.jobportal.DTO.GeneralDTO;
import com.pixelnx.sam.jobportal.DTO.MonthDTO;
import com.pixelnx.sam.jobportal.DTO.MyPostJobDTO;
import com.pixelnx.sam.jobportal.DTO.UserRecruiterDTO;
import com.pixelnx.sam.jobportal.DTO.UserSeekerDTO;
import com.pixelnx.sam.jobportal.DTO.YearDTO;
import com.pixelnx.sam.jobportal.DTOCI.FetchGeneralData;
import com.pixelnx.sam.jobportal.DTOCI.JobPostDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelSeekerLogin;
import com.pixelnx.sam.jobportal.DTOCI.MyPostJobDataDTO;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.CustomTextHeader;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.MonthYearPicker;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Recruiter_PostJob_Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private CustomEdittext etCgpa, etPassYear, etJobDescription, etSkillRequired, etSalFrom, etSalTo, etVacancies, etLastDateOfApp;
    private Spinner SpJobType, SpDesignation, SpQualification, SpJobLocation, SpSpecialization, SpExperience, SpPerDuration, SpAreaOfSector;
    private CheckBox cbFtoF, cbWritten, cbTelephonic, cbGD, cbWalkin;
    private CustomButton btnPostJob;
    RecruiterDashboardActivity recruiterDashboardActivity;
    private SharedPrefrence prefrence;
    FetchGeneralData generalDTO;
    FetchGeneralData.Data generalDataDto;
    ModelSeekerLogin userRecruiterDTO = new ModelSeekerLogin();
    private Context mContext;
    private ImageView ivBack;
    private ArrayList<FetchGeneralData.Data.JobType> job_typesList = new ArrayList<>();
    private ArrayList<FetchGeneralData.Data.Desi> designationList = new ArrayList<>();
    private ArrayList<FetchGeneralData.Data.Qualification> qualificationsList = new ArrayList<>();
    private ArrayList<FetchGeneralData.Data.Location> locationsList = new ArrayList<>();
    private ArrayList<FetchGeneralData.Data.Specialization> specializationList = new ArrayList<>();
    private ArrayList<FetchGeneralData.Data.Exp> expList = new ArrayList<>();
    private List<YearDTO> exprinceYearDTOList;
    private List<MonthDTO> exprinceMonthDTOList = new ArrayList<>();
    private ArrayList<FetchGeneralData.Data.Area_of_s> area_of_sectorsList = new ArrayList<>();
    private ArrayList<String> process;
    private ArrayAdapter<FetchGeneralData.Data.JobType> job_type_Adapter;
    private ArrayAdapter<FetchGeneralData.Data.Desi> designation_adapter;
    private ArrayAdapter<FetchGeneralData.Data.Qualification> qualification_Adapter;
    private ArrayAdapter<FetchGeneralData.Data.Location> location_Adapter;
    private ArrayAdapter<FetchGeneralData.Data.Specialization> specialization_Adapter;
    private ArrayAdapter<FetchGeneralData.Data.Exp> exprinceYear_adapter;
    private ArrayAdapter<MonthDTO> exprinceMonth_adapter;
    private ArrayAdapter<FetchGeneralData.Data.Area_of_s> aos_Adapter;

    JSONArray jsonArray = new JSONArray();
    View view;
    private Calendar myCalendar = Calendar.getInstance();
    int flag = 0;
    private CustomTextHeader tvHeader;
    private MyPostJobDataDTO myPostJobDTO;
    String id = "";
    private MonthYearPicker myp;
    private Typeface font;
private  String errorMSG="";

    private ModelLanguageDTO.Data modelLanguageDTO;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_post_job);
        mContext = Recruiter_PostJob_Activity.this;
        prefrence = SharedPrefrence.getInstance(mContext);
        ProjectUtils.initImageLoader(mContext);
        flag = getIntent().getIntExtra(Consts.FLAG, 0);
        font = Typeface.createFromAsset(
                Recruiter_PostJob_Activity.this.getAssets(),
                "Raleway-Light.ttf");

        userRecruiterDTO = prefrence.getUserDTO(AppConstans.SEEKERDTO);
        modelLanguageDTO = prefrence.getLanguage(AppConstans.LANGUAGE).getData();
        errorMSG=modelLanguageDTO.getFill_keyword();
        init();
    }


    private void init() {
        tvHeader = (CustomTextHeader) findViewById(R.id.tvHeader);
        etCgpa = (CustomEdittext) findViewById(R.id.etCgpa);
        etPassYear = (CustomEdittext) findViewById(R.id.etPassYear);
        etJobDescription = (CustomEdittext) findViewById(R.id.etJobDescription);
        etSkillRequired = (CustomEdittext) findViewById(R.id.etSkillRequired);
        etSalFrom = (CustomEdittext) findViewById(R.id.etSalFrom);
        etSalTo = (CustomEdittext) findViewById(R.id.etSalTo);
        etVacancies = (CustomEdittext) findViewById(R.id.etVacancies);
        etLastDateOfApp = (CustomEdittext) findViewById(R.id.etLastDateOfApp);


        SpJobType = (Spinner) findViewById(R.id.SpJobType);
        SpDesignation = (Spinner) findViewById(R.id.SpDesignation);
        SpQualification = (Spinner) findViewById(R.id.SpQualification);
        SpJobLocation = (Spinner) findViewById(R.id.SpJobLocation);
        SpSpecialization = (Spinner) findViewById(R.id.SpSpecialization);
        SpExperience = (Spinner) findViewById(R.id.SpExperience);
        SpPerDuration = (Spinner) findViewById(R.id.SpPerDuration);
        SpAreaOfSector = (Spinner) findViewById(R.id.SpAreaOfSector);

        cbFtoF = (CheckBox) findViewById(R.id.cbFtoF);
        cbWritten = (CheckBox) findViewById(R.id.cbWritten);
        cbTelephonic = (CheckBox) findViewById(R.id.cbTelephonic);
        cbGD = (CheckBox) findViewById(R.id.cbGD);
        cbWalkin = (CheckBox) findViewById(R.id.cbWalkin);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        btnPostJob = (CustomButton) findViewById(R.id.btnPostJob);
        changeLanguage();

        SpJobType.setOnItemSelectedListener(this);
        SpDesignation.setOnItemSelectedListener(this);
        SpQualification.setOnItemSelectedListener(this);
        SpJobLocation.setOnItemSelectedListener(this);
        SpSpecialization.setOnItemSelectedListener(this);
        SpExperience.setOnItemSelectedListener(this);
        SpPerDuration.setOnItemSelectedListener(this);
        SpAreaOfSector.setOnItemSelectedListener(this);

        cbFtoF.setOnClickListener(this);
        cbWalkin.setOnClickListener(this);
        cbGD.setOnClickListener(this);
        cbTelephonic.setOnClickListener(this);
        cbWritten.setOnClickListener(this);

        cbFtoF.setTypeface(font);
        cbWalkin.setTypeface(font);
        cbGD.setTypeface(font);
        cbTelephonic.setTypeface(font);
        cbWritten.setTypeface(font);

        etLastDateOfApp.setOnClickListener(this);
        etPassYear.setOnClickListener(this);
        ivBack.setOnClickListener(this);


        btnPostJob.setOnClickListener(this);

        exprinceYearDTOList = new ArrayList<>();
        exprinceYearDTOList.add(new YearDTO("0", "----- SELECT EXPERIENCE -----"));
        exprinceYearDTOList.add(new YearDTO("1", "Fresher"));
        exprinceYearDTOList.add(new YearDTO("2", "06 Months"));
        exprinceYearDTOList.add(new YearDTO("3", "1 Year"));
        exprinceYearDTOList.add(new YearDTO("4", "1.5 Year"));
        exprinceYearDTOList.add(new YearDTO("5", "2 Year"));
        exprinceYearDTOList.add(new YearDTO("6", "2.5 Year"));
        exprinceYearDTOList.add(new YearDTO("7", "3 Year"));
        exprinceYearDTOList.add(new YearDTO("8", "3+ Year"));


        exprinceMonthDTOList = new ArrayList<>();
        exprinceMonthDTOList.add(new MonthDTO("0", "--SELECT--"));
        exprinceMonthDTOList.add(new MonthDTO("1", "Year"));
        exprinceMonthDTOList.add(new MonthDTO("2", "Month"));
        exprinceMonthDTOList.add(new MonthDTO("3", "Day"));
        exprinceMonthDTOList.add(new MonthDTO("4", "Hour"));

        etCgpa.setFilters(new InputFilter[]{
                new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                    int beforeDecimal = 2, afterDecimal = 2;

                    @Override
                    public CharSequence filter(CharSequence source, int start, int end,
                                               Spanned dest, int dstart, int dend) {
                        String temp = etCgpa.getText() + source.toString();

                        if (temp.equals(".")) {
                            return "0.";
                        } else if (temp.toString().indexOf(".") == -1) {
                            // no decimal point placed yet
                            if (temp.length() > beforeDecimal) {
                                return "";
                            }
                        } else {
                            temp = temp.substring(temp.indexOf(".") + 1);
                            if (temp.length() > afterDecimal) {
                                return "";
                            }
                        }

                        return super.filter(source, start, end, dest, dstart, dend);
                    }
                }
        });

        getData();

        myp = new MonthYearPicker(this);
        myp.build(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (myp.getSelectedYear() != 0) {
                    etPassYear.setText("" + myp.getSelectedYear());

                } else {
                    ProjectUtils.showToast(mContext, getResources().getString(R.string.cant_futuer_date));
                }
            }
        }, null);

    }

    private void changeLanguage() {
        CustomTextview lgjobType, lgDesi, lgjQua, lgLoc, lgyearp, lg_pcgpa, lgspli, lgaos, lgexp, lgRange, lgvac, lgtech, lgLast, lgPro,lgJobDesc;
        lgjobType = (CustomTextview) findViewById(R.id.lgjobType);
        lgDesi = (CustomTextview) findViewById(R.id.lgDesi);
        lgjQua = (CustomTextview) findViewById(R.id.lgjQua);
        lgLoc = (CustomTextview) findViewById(R.id.lgLoc);
        lgspli = (CustomTextview) findViewById(R.id.lgspli);
        lgaos = (CustomTextview) findViewById(R.id.lgaos);
        lgexp = (CustomTextview) findViewById(R.id.lgexp);
        lgRange = (CustomTextview) findViewById(R.id.lgRange);
        lgvac = (CustomTextview) findViewById(R.id.lgvac);
        lgtech = (CustomTextview) findViewById(R.id.lgtech);
        lgLast = (CustomTextview) findViewById(R.id.lgLast);
        lgPro = (CustomTextview) findViewById(R.id.lgPro);
        lgyearp = (CustomTextview) findViewById(R.id.lgyearp);
        lg_pcgpa = (CustomTextview) findViewById(R.id.lg_pcgpa);
        lgJobDesc = (CustomTextview) findViewById(R.id.lgJobDesc);

        lgjobType.setText(modelLanguageDTO.getJob_type());
        lgDesi.setText(modelLanguageDTO.getDesi());
        lgjQua.setText(modelLanguageDTO.getQua());
        lgLoc.setText(modelLanguageDTO.getJ_location());
        lgyearp.setText(modelLanguageDTO.getP_year());
        lg_pcgpa.setText(modelLanguageDTO.getJob_cgpa());
        lgspli.setText(modelLanguageDTO.getSp());
        lgaos.setText(modelLanguageDTO.getJob_aofs());
        lgexp.setText(modelLanguageDTO.getUser_exp());
        lgRange.setText(modelLanguageDTO.getS_range());
        lgvac.setText(modelLanguageDTO.getEnter_number_of_v());
        lgtech.setText(modelLanguageDTO.getTech());
        lgLast.setText(modelLanguageDTO.getLast_date_a());
        lgPro.setText(modelLanguageDTO.getH_pro());
        lgJobDesc.setText(modelLanguageDTO.getJob_desc1());
    }

    private void getData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppConstans.BASE_URL + AppConstans.FETCH_ALL_CAT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            generalDTO = new Gson().fromJson(response, FetchGeneralData.class);
         showData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);

    }


    public void showData() {
        job_typesList = generalDTO.getData().getJob_type();
        job_typesList.add(0, new FetchGeneralData.Data.JobType(modelLanguageDTO.getJob_type()));
        job_type_Adapter = new ArrayAdapter<FetchGeneralData.Data.JobType>(mContext, android.R.layout.simple_spinner_dropdown_item, job_typesList);
        job_type_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpJobType.setAdapter(job_type_Adapter);

        designationList = generalDTO.getData().getDesi();
        designationList.add(0, new FetchGeneralData.Data.Desi(modelLanguageDTO.getDesi()));
        designation_adapter = new ArrayAdapter<FetchGeneralData.Data.Desi>(mContext, android.R.layout.simple_spinner_dropdown_item, designationList);
        designation_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpDesignation.setAdapter(designation_adapter);

        qualificationsList = generalDTO.getData().getQualification();
        qualificationsList.add(0, new FetchGeneralData.Data.Qualification(modelLanguageDTO.getQua()));
        qualification_Adapter = new ArrayAdapter<FetchGeneralData.Data.Qualification>(mContext, android.R.layout.simple_spinner_dropdown_item, qualificationsList);
        qualification_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpQualification.setAdapter(qualification_Adapter);

        locationsList = generalDTO.getData().getLocation();
        locationsList.add(0, new FetchGeneralData.Data.Location(modelLanguageDTO.getJ_location()));
        location_Adapter = new ArrayAdapter<FetchGeneralData.Data.Location>(mContext, android.R.layout.simple_spinner_dropdown_item, locationsList);
        location_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpJobLocation.setAdapter(location_Adapter);


        area_of_sectorsList = generalDTO.getData().getArea_of_s();
        area_of_sectorsList.add(0, new FetchGeneralData.Data.Area_of_s(modelLanguageDTO.getJob_aofs()));
        aos_Adapter = new ArrayAdapter<FetchGeneralData.Data.Area_of_s>(mContext, android.R.layout.simple_spinner_dropdown_item, area_of_sectorsList);
        aos_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpAreaOfSector.setAdapter(aos_Adapter);

        specializationList = generalDTO.getData().getSpecialization();
        specializationList.add(0, new FetchGeneralData.Data.Specialization(modelLanguageDTO.getSelect_sp()));
        specialization_Adapter = new ArrayAdapter<FetchGeneralData.Data.Specialization>(mContext, android.R.layout.simple_spinner_dropdown_item, specializationList);
        specialization_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpSpecialization.setAdapter(specialization_Adapter);

        expList = generalDTO.getData().getExp();
        exprinceYear_adapter = new ArrayAdapter<FetchGeneralData.Data.Exp>(mContext, android.R.layout.simple_spinner_dropdown_item, expList);
        exprinceYear_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpExperience.setAdapter(exprinceYear_adapter);

        exprinceMonth_adapter = new ArrayAdapter<MonthDTO>(mContext, android.R.layout.simple_spinner_dropdown_item, exprinceMonthDTOList);
        exprinceMonth_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpPerDuration.setAdapter(exprinceMonth_adapter);

        if (flag == 1) {
            btnPostJob.setText(getResources().getString(R.string.post_job));
            tvHeader.setText(modelLanguageDTO.getJob_post_heading1());
        } else if (flag == 2) {
            id = getIntent().getStringExtra(Consts.JOB_ID);
            btnPostJob.setText(getResources().getString(R.string.update_job));
            tvHeader.setText(modelLanguageDTO.getEdit_job_post_heading());
            getPostJob();
        }
    }


    public void Submit() {

        if (NetworkManager.isConnectToInternet(mContext)) {
            submitForm();

        } else {
            ProjectUtils.showToast(mContext, getResources().getString(R.string.internet_connection));
        }
    }

    /*public boolean Validate() {
        if (job_typesList.get(SpJobType.getSelectedItemPosition()).getName().equalsIgnoreCase("----- SELECT JOB TYPE -----")) {
            ProjectUtils.showToast(mContext, "Please select Job type");
            return false;
        } else if (designationList.get(SpDesignation.getSelectedItemPosition()).getName().equalsIgnoreCase("----- SELECT DESIGNATION -----")) {
            ProjectUtils.showToast(mContext, "Please select Designation");
            return false;
        } else if (qualificationsList.get(SpQualification.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getQua())) {
            ProjectUtils.showToast(mContext, "Please select Qualification");
            return false;
        } else if (locationsList.get(SpJobLocation.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getJ_location())) {
            ProjectUtils.showToast(mContext, "Please select Location");
            return false;
        } else if (etPassYear.getText().toString().trim().equals("")) {
            ProjectUtils.showToast(mContext, "Please enter year of passing");
            etPassYear.requestFocus();
            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etCgpa)) {
            etCgpa.setError("Please enter percentage");
            etCgpa.requestFocus();
            return false;
        } else if (specializationList.get(SpSpecialization.getSelectedItemPosition()).getName().equalsIgnoreCasemodelLanguageDTO.getSelect_sp())) {
            ProjectUtils.showToast(mContext, "Please select Specialization");
            return false;
        } else if (area_of_sectorsList.get(SpAreaOfSector.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getJob_aofs())) {
            ProjectUtils.showToast(mContext, "Please select AREA");
            return false;
        } else if (exprinceYearDTOList.get(SpExperience.getSelectedItemPosition()).getYear().equalsIgnoreCase("----- SELECT EXPERIENCE -----")) {
            ProjectUtils.showToast(mContext, "Please select Experience");
            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etSalFrom)) {
            etSalFrom.setError("Please enter min sal");
            etSalFrom.requestFocus();

            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etSalTo)) {
            etSalTo.setError("Please enter max sal");
            etSalTo.requestFocus();
            return false;
        } else if (exprinceMonthDTOList.get(SpPerDuration.getSelectedItemPosition()).getMonth().equalsIgnoreCase("-- SELECT --")) {
            ProjectUtils.showToast(mContext, "Please select Duration");
            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etVacancies)) {
            etVacancies.setError("Please enter no. of vacancies");
            etVacancies.requestFocus();
            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etSkillRequired)) {
            etSkillRequired.setError("Please enter Required Skills");
            etSkillRequired.requestFocus();
            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etLastDateOfApp)) {
            ProjectUtils.showToast(mContext, "Please enter last date of application");
            etLastDateOfApp.requestFocus();
            return false;
        } else if (!(cbFtoF.isChecked() || cbWritten.isChecked() || cbTelephonic.isChecked() || cbGD.isChecked() || cbWalkin.isChecked())) {
            ProjectUtils.showToast(mContext, "please select hiring process");
            return false;
        } else if (!ProjectUtils.IsEditTextValidation(etJobDescription)) {
            etJobDescription.setError("Please enter Job Description");
            etJobDescription.requestFocus();
            return false;
        }

        return true;
    }*/

    public void submitForm() {
        if (!validateJobType()) {
            return;
        } else if (!validateDesignation()) {
            return;
        } else if (!validateQualification()) {
            return;
        } else if (!validateLocation()) {
            return;
        } else if (!validatePassing()) {
            return;
        } else if (!validateCgpa()) {
            return;
        } else if (!validateSpeciailization()) {
            return;
        } else if (!validateArea()) {
            return;
        }/* else if (!validateExperience()) {
            return;
        }*/ else if (!validateSalFrom()) {
            return;
        } else if (!validateSalTo()) {
            return;
        } else if (!validatePerValue()) {
            return;
        } else if (!validateVacancies()) {
            return;
        } else if (!validateSkills()) {
            return;
        } else if (!validateLastDate()) {
            return;
        } else if (!validateHiring()) {
            return;
        } else if (!validateJobDescription()) {
            return;
        } else {

            postJob();

        }
    }

    public boolean validateJobType() {
        if (job_typesList.get(SpJobType.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getJob_type())) {
            ProjectUtils.showToast(mContext, errorMSG);
            return false;
        } else {
            return true;
        }
    }

    public boolean validateDesignation() {
        if (designationList.get(SpDesignation.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getDesi())) {
            ProjectUtils.showToast(mContext,errorMSG);
            return false;
        } else {
            return true;
        }
    }

    public boolean validateQualification() {
        if (qualificationsList.get(SpQualification.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getQua())) {
            ProjectUtils.showToast(mContext,errorMSG);
            return false;
        } else {
            return true;
        }
    }

    public boolean validateLocation() {
        if (locationsList.get(SpJobLocation.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getJ_location())) {
            ProjectUtils.showToast(mContext, errorMSG);
            return false;
        } else {
            return true;
        }
    }

    public boolean validatePassing() {
        if (etPassYear.getText().toString().trim().length() <= 0) {
            ProjectUtils.showToast(mContext,errorMSG);
            etPassYear.requestFocus();
            return false;
        } else {
            etPassYear.clearFocus();
            return true;
        }
    }

    public boolean validateCgpa() {
        if (etCgpa.getText().toString().trim().length() <= 0) {
            etCgpa.setError(errorMSG);
            etCgpa.requestFocus();
            return false;
        } else {
            etCgpa.clearFocus();
            return true;
        }
    }

    public boolean validateSpeciailization() {
        if (specializationList.get(SpSpecialization.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getSelect_sp())) {
            ProjectUtils.showToast(mContext, errorMSG);
            return false;
        } else {
            return true;
        }
    }

    public boolean validateArea() {
        if (area_of_sectorsList.get(SpAreaOfSector.getSelectedItemPosition()).getName().equalsIgnoreCase(modelLanguageDTO.getJob_aofs())) {
            ProjectUtils.showToast(mContext,errorMSG);
            return false;
        } else {
            return true;
        }
    }

   /* public boolean validateExperience() {
      if (exprinceYearDTOList.get(SpExperience.getSelectedItemPosition()).get().equalsIgnoreCase("----- SELECT EXPERIENCE -----")) {
            ProjectUtils.showToast(mContext, getResources().getString(R.string.val_experience));
            return false;
        } else {
        return true;
         }
    }
*/
    public boolean validateSalFrom() {
        if (etSalFrom.getText().toString().trim().length() <= 0) {
            etSalFrom.setError(errorMSG);
            etSalFrom.requestFocus();
            return false;
        } else {
            etSalFrom.clearFocus();
            return true;
        }
    }

    public boolean validateSalTo() {
        if (etSalTo.getText().toString().trim().length() <= 0) {
            etSalTo.setError(errorMSG);
            etSalTo.requestFocus();
            return false;
        } else {
            int min = Integer.parseInt(etSalFrom.getText().toString().trim());
            int max = Integer.parseInt(etSalTo.getText().toString().trim());
            if (min < max) {
                etSalTo.clearFocus();
                return true;
            } else {
                etSalTo.setError(errorMSG);
                etSalTo.requestFocus();
                return false;
            }

        }
    }

    public boolean validatePerValue() {
        if (exprinceMonthDTOList.get(SpPerDuration.getSelectedItemPosition()).getMonth().equalsIgnoreCase("--SELECT--")) {
            ProjectUtils.showToast(mContext, errorMSG);
            return false;
        } else {
            return true;
        }
    }

    public boolean validateVacancies() {
        if (etVacancies.getText().toString().trim().length() <= 0) {
            etVacancies.setError(errorMSG);
            etVacancies.requestFocus();
            return false;
        } else {
            etVacancies.clearFocus();
            return true;
        }
    }

    public boolean validateSkills() {
        if (etSkillRequired.getText().toString().trim().length() <= 0) {
            etSkillRequired.setError(errorMSG);
            etSkillRequired.requestFocus();
            return false;
        } else {
            etSkillRequired.clearFocus();
            return true;
        }
    }

    public boolean validateLastDate() {
        if (etLastDateOfApp.getText().toString().trim().length() <= 0) {
            ProjectUtils.showToast(mContext,errorMSG);
            etLastDateOfApp.requestFocus();
            return false;
        } else {
            etLastDateOfApp.clearFocus();
            return true;
        }
    }

    public boolean validateHiring() {
        if (!(cbFtoF.isChecked() || cbWritten.isChecked() || cbTelephonic.isChecked() || cbGD.isChecked() || cbWalkin.isChecked())) {
            ProjectUtils.showToast(mContext, errorMSG);
            return false;
        } else {

            return true;
        }

    }

    public boolean validateJobDescription() {
        if (etJobDescription.getText().toString().trim().length() <= 0) {
            etJobDescription.setError(errorMSG);
            etJobDescription.requestFocus();
            return false;
        } else {
            etJobDescription.clearFocus();
            return true;
        }
    }


    public void postJob() {
        ProjectUtils.showProgressDialog(mContext, false, "Please wait");
        String url = "";
        if (flag == 2)
            url = AppConstans.BASE_URL + AppConstans.UPDATE_JOB_POST;
        else
            url = AppConstans.BASE_URL + AppConstans.POST_JOB_API;



        Log.e("Hello",""+url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("POST JOB", response.toString());

                        JobPostDTO commonDTO = new JobPostDTO();
                        commonDTO = new Gson().fromJson(response, JobPostDTO.class);

                        if (commonDTO.getStaus().equalsIgnoreCase("true")) {
                            ProjectUtils.pauseProgressDialog();
                            if (commonDTO.getMessage().equalsIgnoreCase("please pay")){
                                ProjectUtils.showDialog(Recruiter_PostJob_Activity.this, userRecruiterDTO.getData().getEmail(), userRecruiterDTO.getData().getMno());

                            }else {
                                finish();
                            }


                        } else {
                            ProjectUtils.showToast(mContext, commonDTO.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Message", "" + error.getNetworkTimeMs() + " -- " + error.getLocalizedMessage() + "--" + error.toString());
                        VolleyError volleyError = new VolleyError();
                        volleyError.getNetworkTimeMs();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (flag == 2) {
                    params.put(Consts.JOB_ID, id);

                } else
                    params.put(AppConstans.RECRUITER_EMAIL, userRecruiterDTO.getData().getEmail());


                params.put(AppConstans.JOB_TYPE, job_typesList.get(SpJobType.getSelectedItemPosition()).getName().trim() + "");

                params.put(AppConstans.DESIGATION, designationList.get(SpDesignation.getSelectedItemPosition()).getName() + "");
                params.put(AppConstans.QUALIFICATION, qualificationsList.get(SpQualification.getSelectedItemPosition()).getName() + "");
                params.put(AppConstans.JOB_LOCATION, locationsList.get(SpJobLocation.getSelectedItemPosition()).getName() + "");
                params.put(AppConstans.YEAR_OF_PASSING, ProjectUtils.getEditTextValue(etPassYear));
                params.put(AppConstans.PRE_CGPA, ProjectUtils.getEditTextValue(etCgpa));
                params.put(AppConstans.SPECIALIZATION, specializationList.get(SpSpecialization.getSelectedItemPosition()).getName() + "");
                params.put(AppConstans.AREA_OF_SECTOR, area_of_sectorsList.get(SpAreaOfSector.getSelectedItemPosition()).getName() + "");
                params.put(AppConstans.EXPERIENCE, expList.get(SpExperience.getSelectedItemPosition()).getName() + "");
                params.put(AppConstans.VACANCIES, ProjectUtils.getEditTextValue(etVacancies));
                params.put(AppConstans.LAST_DATE, ProjectUtils.getEditTextValue(etLastDateOfApp));
                params.put(AppConstans.SALARY_RANGE, exprinceMonthDTOList.get(SpPerDuration.getSelectedItemPosition()).getMonth() + "");
                params.put(AppConstans.MIN_SAL, ProjectUtils.getEditTextValue(etSalFrom));
                params.put(AppConstans.MAX_SAL, ProjectUtils.getEditTextValue(etSalTo));
                params.put(AppConstans.R_ID, userRecruiterDTO.getData().getEmail());
                //params.put(AppConstans.POST_DATE, userRecruiterDTO.getData().getEmail());
                params.put(AppConstans.TECHNOLOGY, ProjectUtils.getEditTextValue(etSkillRequired));
                params.put(AppConstans.JOB_DISCRIPTION, ProjectUtils.getEditTextValue(etJobDescription));

                JSONArray jsonArray = new JSONArray();

                if (cbFtoF.isChecked()) {
                    //    String p1 = cbFtoF.getText().toString().trim();
                    String p1 = "1";
                    jsonArray.put(p1);
                    params.put(AppConstans.TECHNICAL_ROUND, "yes");
                }

                if (cbGD.isChecked()) {
                    //  String p2 = cbGD.getText().toString().trim();
                    String p2 = "2";
                    jsonArray.put(p2);
                    params.put(AppConstans.GROUP_DISCUSSION, "yes");
                }

                if (cbTelephonic.isChecked()) {
                    //    String p3 = cbTelephonic.getText().toString().trim();
                    String p3 = "3";
                    jsonArray.put(p3);
                    params.put(AppConstans.HR_ROUND, "yes");
                }

                if (cbWalkin.isChecked()) {
                    // String p4 = cbWalkin.getText().toString().trim();
                    String p4 = "4";
                    jsonArray.put(p4);

                }

                if (cbWritten.isChecked()) {
                    //  String p5 = cbWritten.getText().toString().trim();
                    String p5 = "5";
                    jsonArray.put(p5);
                    params.put(AppConstans.WRITTEN_TEST, "yes");
                }


                //params.put(Consts.PROCESS, jsonArray.toString());

                Log.e("POST_JOB", params.toString());

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);

    }

    public void getPostJob() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.JOB_EDIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("GET_JOB", response.toString());

                        myPostJobDTO = new MyPostJobDataDTO();
                        myPostJobDTO = new Gson().fromJson(response, MyPostJobDataDTO.class);

                        if (myPostJobDTO.getStaus().equalsIgnoreCase("true")) {
                            ProjectUtils.showToast(mContext, myPostJobDTO.getMessage());
                            viewJob();
                        } else {
                            ProjectUtils.showToast(mContext, myPostJobDTO.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put(AppConstans.JOB_ID, id);

                Log.e("GET_JOB", params.toString());

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPostJob:

                Submit();
                break;
            case R.id.etPassYear:
                myp.show();
                //   ProjectUtils.datePicker(myCalendar, mContext, etPassYear, false);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.etLastDateOfApp:
                ProjectUtils.datePicker(myCalendar, mContext, etLastDateOfApp, true);
                break;
        }

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ((TextView) adapterView.getChildAt(0)).setTypeface(font);
        ((TextView) adapterView.getChildAt(0)).setTextSize(14);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }

    public void viewJob() {
try {
    for (int i = 0; i < locationsList.size(); i++) {
        if (locationsList.get(i).getName().equalsIgnoreCase(myPostJobDTO.getData().getJob_location())) {
            SpJobLocation.setSelection(i);
            break;
        }
    }
    for (int i = 0; i < job_typesList.size(); i++) {
        if (job_typesList.get(i).getName().trim().equalsIgnoreCase(myPostJobDTO.getData().getJob_type())) {
            SpJobType.setSelection(i);
            break;
        }
    }
    for (int i = 0; i < qualificationsList.size(); i++) {
        if (qualificationsList.get(i).getName().equalsIgnoreCase(myPostJobDTO.getData().getQualification())) {
            SpQualification.setSelection(i);
            break;
        }
    }
    for (int i = 0; i < specializationList.size(); i++) {
        if (specializationList.get(i).getName().equalsIgnoreCase(myPostJobDTO.getData().getSpecialization())) {
            SpSpecialization.setSelection(i);
            break;
        }
    }
    for (int i = 0; i < area_of_sectorsList.size(); i++) {
        if (area_of_sectorsList.get(i).getName().equalsIgnoreCase(myPostJobDTO.getData().getArea_of_sector())) {
            SpAreaOfSector.setSelection(i);
            break;
        }
    }
    for (int i = 0; i < designationList.size(); i++) {
        if (designationList.get(i).getName().equalsIgnoreCase(myPostJobDTO.getData().getDesignation())) {
            SpDesignation.setSelection(i);
            break;
        }
    }

    for (int i = 0; i < expList.size(); i++) {
        if (expList.get(i).getName().equalsIgnoreCase(myPostJobDTO.getData().getExp())) {
            SpExperience.setSelection(i);
            break;
        }
    }
}catch (IndexOutOfBoundsException e){
    e.printStackTrace();
}
     /*   for (int i = 0; i < exprinceMonthDTOList.size(); i++) {
            if (exprinceMonthDTOList.get(i).getMonth().equalsIgnoreCase(myPostJobDTO.getData().getSalary_range())) {
                SpPerDuration.setSelection(i);
                break;
            }
        }*/

        etPassYear.setText(myPostJobDTO.getData().getYear_of_passing());
        etCgpa.setText(myPostJobDTO.getData().getPre_cgpa());
        etSalFrom.setText(myPostJobDTO.getData().getMin());
        etSalTo.setText(myPostJobDTO.getData().getMax());
        etVacancies.setText(myPostJobDTO.getData().getNumber_of_vacancies());
        etSkillRequired.setText(myPostJobDTO.getData().getTechnology());
        etLastDateOfApp.setText(myPostJobDTO.getData().getLasr_date_application());
        etJobDescription.setText(myPostJobDTO.getData().getJob_desc());
        // process = new ArrayList<>();
        //   process = myPostJobDTO.getData().getProcess();

        if (myPostJobDTO.getData().getTechnical_round().equalsIgnoreCase("yes")) {
            cbFtoF.setChecked(true);
        }
        if (myPostJobDTO.getData().getWritten_test().equalsIgnoreCase("yes")) {
            cbWritten.setChecked(true);
        }
        if (myPostJobDTO.getData().getTechnical_round().equalsIgnoreCase("yes")) {
            cbTelephonic.setChecked(true);
        }
        if (myPostJobDTO.getData().getTechnical_round().equalsIgnoreCase("yes")) {
            cbGD.setChecked(true);
        }

    }

}