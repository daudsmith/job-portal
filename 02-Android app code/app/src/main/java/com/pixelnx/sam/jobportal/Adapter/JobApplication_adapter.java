package com.pixelnx.sam.jobportal.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Activity.JobsView;
import com.pixelnx.sam.jobportal.Activity.Recruiter_PostJob_Activity;
import com.pixelnx.sam.jobportal.DTO.CommonDTO;
import com.pixelnx.sam.jobportal.DTO.JobApplicationDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterPostJobs;
import com.pixelnx.sam.jobportal.Fragment.JobApplication;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by shubham on 28/8/17.
 */

public class JobApplication_adapter extends RecyclerView.Adapter<JobApplication_adapter.JobAppHolder> {

    private ArrayList<ModelRecruiterPostJobs.Data> jobApplicationDTOList = new ArrayList<>();
    private Context mContext;
    private CommonDTO commonDTO;
    SharedPrefrence sharedPrefrence;
    int lastPosition = -1;
    private String searchString = "";
    private JobApplication jobApplication;
    private String editString="",deleteString="",dialogDelete="";
    private ArrayList<ModelRecruiterPostJobs.Data> tempcontactDTOList;


    public JobApplication_adapter(ArrayList<ModelRecruiterPostJobs.Data> jobApplicationDTOList, Context mContext, JobApplication jobApplication) {
        this.jobApplicationDTOList = jobApplicationDTOList;
        this.mContext = mContext;
        this.jobApplication = jobApplication;
        sharedPrefrence = SharedPrefrence.getInstance(mContext);

        editString=sharedPrefrence.getLanguage(AppConstans.LANGUAGE).getData().getEdit_job_btn();
        deleteString=sharedPrefrence.getLanguage(AppConstans.LANGUAGE).getData().getDelete_keyword();
        dialogDelete=sharedPrefrence.getLanguage(AppConstans.LANGUAGE).getData().getDelete_job_msg();

        this.tempcontactDTOList = new ArrayList<ModelRecruiterPostJobs.Data >();
        this.tempcontactDTOList.addAll(jobApplicationDTOList);

    }

    @Override
    public JobAppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_jobapplication, parent, false);
        JobAppHolder jobAppHolder = new JobAppHolder(view);

        return jobAppHolder;

    }

    @Override
    public void onBindViewHolder(final JobAppHolder holder, final int position) {

        holder.tvCount.setText("");

        holder.tvSpecialn.setText(jobApplicationDTOList.get(position).getDesignation());
        holder.tvJobDescription.setText("" + jobApplicationDTOList.get(position).getJob_desc());
holder.tvDeleteJOB.setText(deleteString);

            String country = jobApplicationDTOList.get(position).getDesignation().toLowerCase(Locale.getDefault());

            if (country.contains(searchString)) {
                int startPos = country.indexOf(searchString);
                int endPos = startPos + searchString.length();

                Spannable spanText = Spannable.Factory.getInstance().newSpannable(holder.tvSpecialn.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
                spanText.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.lightgray)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvSpecialn.setText(spanText, TextView.BufferType.SPANNABLE);
            }





        holder.cvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent in = new Intent(mContext, JobsView.class);
                    in.putExtra(Consts.RECRUITER_ID, sharedPrefrence.getUserDTO(AppConstans.SEEKERDTO).getData().getEmail());
                    in.putExtra(Consts.JOB_ID, jobApplicationDTOList.get(position).getId());
                    in.putExtra(Consts.ET_NAME, holder.tvSpecialn.getText().toString().trim());
                    mContext.startActivity(in);
                   // ProjectUtils.showToast(mContext,"Not Available");
               /*  ProjectUtils.showSweetDialog(mContext, "Applied seeker not found.", "", "Ok", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    }, SweetAlertDialog.WARNING_TYPE);*/
                }

        });
       holder.tvDeleteJOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lastPosition = position;
                ProjectUtils.showAlertDialog(mContext, deleteString, dialogDelete, "OK", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteJOB(position);
                        dialogInterface.dismiss();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
            }
        });
        holder.tveditJOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(mContext, Recruiter_PostJob_Activity.class);
                in.putExtra(Consts.JOB_ID, jobApplicationDTOList.get(position).getId());
                in.putExtra(Consts.FLAG, 2);
                mContext.startActivity(in);
            }
        });
setAnimation(holder.cvMain,position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_from_bottom);
            animation.setDuration(700);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return jobApplicationDTOList.size();
    }

    public void filter(String charText) {

        this.searchString = charText;
        charText = charText.toLowerCase(Locale.getDefault());
        jobApplicationDTOList.clear();
        if (charText.length() == 0) {
            jobApplicationDTOList.addAll(tempcontactDTOList);
        } else {
            for (ModelRecruiterPostJobs.Data data : tempcontactDTOList) {
                if (data.getDesignation().toLowerCase(Locale.getDefault()).contains(charText)) {
                    jobApplicationDTOList.add(data);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class JobAppHolder extends RecyclerView.ViewHolder {
        CustomTextview tvCount, tvDeleteJOB, tveditJOB, tvJobDescription;
        CustomTextviewBold tvSpecialn;
        LinearLayout llMain, llNext;
        LinearLayout cvMain;

        public JobAppHolder(View itemView) {
            super(itemView);
            tvSpecialn = (CustomTextviewBold) itemView.findViewById(R.id.tvSpecialn);
            llNext = (LinearLayout) itemView.findViewById(R.id.llNext);
            tvCount = (CustomTextview) itemView.findViewById(R.id.tvCount);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            tvDeleteJOB = (CustomTextview) itemView.findViewById(R.id.tvDeleteJOB);
            tveditJOB = (CustomTextview) itemView.findViewById(R.id.tveditJOB);
            tvJobDescription = (CustomTextview) itemView.findViewById(R.id.tvJobDescription);
            cvMain = (LinearLayout) itemView.findViewById(R.id.cvMain);

        }
    }

    public void deleteJOB(final int position) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.DELETE_JOB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("STATUS JOBS", response.toString());
                            commonDTO = new Gson().fromJson(response, CommonDTO.class);
                            if (commonDTO.getStaus().equalsIgnoreCase("true")) {
                                ProjectUtils.showToast(mContext, commonDTO.getMessage());
                                jobApplicationDTOList.remove(lastPosition);
                                if (jobApplicationDTOList.size() == 0) {
                                    jobApplication.llIcon.setVisibility(View.VISIBLE);
                                } else {
                                    jobApplication.llIcon.setVisibility(View.GONE);
                                }
                                notifyDataSetChanged();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Consts.RECRUITER_ID, sharedPrefrence.getUserDTO(AppConstans.SEEKERDTO).getData().getEmail());
                params.put(Consts.JOB_ID, jobApplicationDTOList.get(position).getId());

                Log.e("value", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);


    }


}
