package com.pixelnx.sam.jobportal.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pixelnx.sam.jobportal.Activity.RecruiterDashboardActivity;
import com.pixelnx.sam.jobportal.DTO.CommonDTO;
import com.pixelnx.sam.jobportal.DTO.UserRecruiterDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelSeekerLogin;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RecruiterChangePassFragment extends Fragment implements View.OnClickListener {
    private CustomEdittext etOldPassword, etNewPassword, etConfirmNewPassword;
    CustomButton UpdateBtn;
    private SharedPrefrence prefrence;
    ModelSeekerLogin userRecruiterDTO;
    ModelLanguageDTO.Data data;

    private AdView mAdView;
    private View adMobView;
    View view;
    RecruiterDashboardActivity recruiterDashboardActivity;

    private static String TAG = RecruiterChangePassFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recruiter_change_pass, container, false);

        prefrence = SharedPrefrence.getInstance(getActivity());
        recruiterDashboardActivity.tvJobs.setVisibility(View.VISIBLE);
        recruiterDashboardActivity.search_icon.setVisibility(View.INVISIBLE);
        recruiterDashboardActivity.filter_icon.setVisibility(View.INVISIBLE);
        userRecruiterDTO = prefrence.getUserDTO(AppConstans.SEEKERDTO);
        data = prefrence.getLanguage(AppConstans.LANGUAGE).getData();


        etOldPassword = (CustomEdittext) view.findViewById(R.id.etOldPassword);
        etNewPassword = (CustomEdittext) view.findViewById(R.id.etNewPassword);
        etConfirmNewPassword = (CustomEdittext) view.findViewById(R.id.etConfirmNewPassword);
        UpdateBtn = (CustomButton) view.findViewById(R.id.UpdateBtn);

        UpdateBtn.setOnClickListener(this);
        adMobView = view.findViewById(R.id.adMobView);
        //  mAdView = (AdView) view.findViewById(R.id.adView);changeLanguage
        try {
            changeLanguage(view);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  ProjectUtils.showAdd(getActivity(), mAdView, "ca-app-pub-3940256099942544/6300978111", adMobView);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.UpdateBtn:
                Submit();
                break;
        }
    }

    private void changeLanguage(View view) throws Exception {

        CustomTextview lgOldPass, lgNewPass, lgConfirmP;

        lgConfirmP = (CustomTextview) view.findViewById(R.id.lgConfirmP);
        lgOldPass = (CustomTextview) view.findViewById(R.id.lgOldPass);
        lgNewPass = (CustomTextview) view.findViewById(R.id.lgNewPass);

        recruiterDashboardActivity.tvJobs.setText(data.getUser_password_change());
        lgConfirmP.setText(data.getValidation_cps_empty());
        lgOldPass.setText(data.getOld_ps_enter());
        lgNewPass.setText(data.getPassword_login());
    }


    private void Submit() {
        if (!passwordValidation()) {
            return;
        } else if (!checkpass()) {
            return;
        } else {
            if (NetworkManager.isConnectToInternet(getActivity())) {
                updatePassword();


            } else {
                ProjectUtils.showToast(getActivity(), getResources().getString(R.string.internet_connection));
            }
        }
    }


    public boolean passwordValidation() {
        if (!ProjectUtils.IsPasswordValidation(etOldPassword.getText().toString().trim())) {
            etOldPassword.setError(data.getValidation_password_short());
            etOldPassword.requestFocus();
            return false;
        } else if (!ProjectUtils.IsPasswordValidation(etNewPassword.getText().toString().trim())) {
            etNewPassword.setError(data.getValidation_password_short()
            );
            etNewPassword.requestFocus();
            return false;
        } else
            return true;

    }

    private boolean checkpass() {
        if (etNewPassword.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), data.getValidaton_password_empty(), Toast.LENGTH_SHORT).show();
            return false;
        } else if (etConfirmNewPassword.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), data.getValidation_password_short(), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!etNewPassword.getText().toString().trim().equals(etConfirmNewPassword.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Password and Confirm Password not Matched", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void updatePassword() {
        ProjectUtils.showProgressDialog(getActivity(), false, "Please wait...");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.PASSWORD_CHANGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ProjectUtils.pauseProgressDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("staus").equalsIgnoreCase("true")) {
                      ProjectUtils.showToast(getActivity(),""+jsonObject.getString("message"));

                            }
                        } catch (Exception e) {

                        }
                        Log.e(TAG, "response: " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProjectUtils.pauseProgressDialog();
                        Log.e("error_on_changepass_api", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstans.TYPE, "recruiter");
                params.put(AppConstans.EMAIL, userRecruiterDTO.getData().getEmail());
                params.put(AppConstans.OLD_PS, ProjectUtils.getEditTextValue(etOldPassword));
                params.put(AppConstans.PASSWORD, ProjectUtils.getEditTextValue(etNewPassword));
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        recruiterDashboardActivity = (RecruiterDashboardActivity) activity;


    }


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
