package com.pixelnx.sam.jobportal.utils;

public interface AppConstans {
   String JOB_PORTAL = "JobPortal";
   // public static String BASE_URL = "http://jobportal.kamleshyadav.net/api/";
    public static String BASE_URL = "http://pixelpages.net/job_portal/api/";

    /***CI API DETAILS  * * */
    String ALL_JOB = "all_job";
    String LOGIN = "login";
    String SIGNUP = "signUp";
    String SINGLE_JOB_DISPLAY = "single_job";
    String SEARCH_BY_KEYWORD = "search_text";
    String RECRUITER_POST_JOBS = "myjobs";
    String MY_APPLIED_JOB = "my_applied_job";
    String APPLIED_SEEKER = "applied_seeker";
    String SEEKER_INFO = "seeker_info";
    String FORGOT_PASSWORD = "forgot_password";
    String VIEW_APPLIED_SEEKER = "view_applied_seeker";
    String RECRUITER_PROFILE_UPDATE = "recruiter_profile_update";
    String FETCH_ALL_CAT = "fetch_all_cat";
    String JOB_EDIT = "job_edit";
    String APPLAY_JOB  = "applay_job";
    String S_EMAIL = "s_email";
    String UPDATE_JOB_POST="update_job_post";
    String USER_PROFILE_UPDATE ="seeker_profile";
    String SEARCH_BY_FILTER = "left_filter";
    String LANGUAGE ="language" ;
    String SUCCESS_PAYMENT="payu_success";
    String CANCELED_PAYMENT="payu_cancel";
    String PLAN ="plans" ;
    String PASSWORD_CHANGE ="password_change" ;
    String PAYPAL_SUCCESS="paypal_success";
    String RECRUITER_INFO="recruiter_info";
    String DELETE_JOB="delete_job";
    String GOOGLE_LOGIN="google_login";
    String SET_RATING="set_rating";

    /*-***CI API DETAILS

     * * */


 /*
     * Api key for send data
     * */

    String GOOGLE_ID = "google_id";
    String EMAIL = "email";
    String PASSWORD = "ps";
    String TYPE = "type";
    String NAME = "name";
    String MOBILE_NO = "mno";
    String SEARCH_KEYWORD = "search_txt";
    String SEEKERDTO = "seeker_info";

    String JOB_ID = "job_id";
    String SEEKER_EMAIL = "seeker_email";
    String ORG_TYPE = "org_type";
    String LOCATION = "location";
    String P_LOCAION = "p_locaion";
    String ADDRESS = "address";
    String WEBSITE = "website";
    String DISCRIPTION = "des";
    String IMG = "img";
    String AVTAR = "avater";

    String POST_METHOD = "post";
    String POST_JOB_API = "job_post";
    String JOB_TYPE = "job_type";
    String RECRUITER_EMAIL = "r_email";
    String RE_EMAIL = "recruiter_email";
    String JOB_LOCATION = "job_location";
    String DESIGATION = "designation";
    String QUALIFICATION = "qualification";
    String QUA = "qua";
    String YEAR_OF_PASSING = "year_of_passing";
    String PRE_CGPA = "pre_cgpa";
    String CGPA = "cgpa";
    String SPECIALIZATION = "specialization";
    String AREA_OF_SECTOR = "area_of_sector";
    String EXPERIENCE = "exp";
    String VACANCIES = "number_of_vacancies";
    String LAST_DATE = "lasr_date_application";
    String SALARY_RANGE = "salary_range";
    String MIN_SAL = "min";
    String MAX_SAL = "max";
    String R_ID = "r_id";
    String POST_DATE = "post_date";
    String TECHNOLOGY = "technology";
    String JOB_DISCRIPTION = "job_desc";
    String WRITTEN_TEST = "written_test";
    String GROUP_DISCUSSION ="group_discussion" ;
    String TECHNICAL_ROUND = "technical_round";
    String HR_ROUND = "hr_round";
    String META_DESC = "meta_desc";
    String META_KEYWORD = "meta_keyword";
    String AUTHOR = "author";;
    String CURRENT_ADDRESS = "current_address";
    String P_YEAR = "p_year";
    String GENDER = "gender";
    String AOFS = "aofs";
    String PRO_PIC_UPLOAD="pro_pic_upload";
    String COUNTER = "counter";
    public static String ACTIVE_JOBS_DTO = "active_jobs_dto";
    String IS_APPLIED ="is_applied" ;
    String FROM = "from";
    String KEYWORD = "keyword";
    String AMOUNT = "amt";
    String OLD_PS = "old_ps";
    String PS = "ps";
    String TOKEN = "token";
    String PLAN_ID = "plan_id";
    String PAYPAL = "paypal";
    String P_ID = "p_id";
    String RATING="rating";
    String COMMENT="comment";


    /*
     * Api key for send data
     * */
    public static String IS_REGISTER_SEEKER = "is_register_seeker";
    public static String IS_REGISTER_RECRUITER = "is_register_recruiter";
    static String SECONDLANGUAGE = "hi";//put you language code here

    String ALL_RECRUITER="all_recruiter";
}
