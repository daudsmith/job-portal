package com.pixelnx.sam.jobportal.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Activity.SeekerDashboardActivity;
import com.pixelnx.sam.jobportal.DTO.CommonDTO;
import com.pixelnx.sam.jobportal.DTO.UserSeekerDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelSeekerLogin;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.HashMap;
import java.util.Map;


public class SeekerChangePassFragment extends Fragment implements View.OnClickListener {
    private CustomEdittext etOldPassword, etNewPassword, etConfirmNewPassword;
    CustomButton UpdateBtn;
    private SharedPrefrence prefrence;
    ModelSeekerLogin userSeekerDTO;
    View view;

    private AdView mAdView;
    private View adMobView;
    private static String TAG = SeekerChangePassFragment.class.getSimpleName();
    SeekerDashboardActivity seekerDashboardActivity;
    ModelLanguageDTO.Data data;
    private String errorMSG = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_seeker_change_pass, container, false);
        prefrence = SharedPrefrence.getInstance(getActivity());
        data = prefrence.getLanguage(AppConstans.LANGUAGE).getData();
        errorMSG = data.getAll_field_required();
        seekerDashboardActivity.tvJobs.setVisibility(View.VISIBLE);
        seekerDashboardActivity.tvJobs.setText(data.getUser_password_change());
        seekerDashboardActivity.search_icon.setVisibility(View.INVISIBLE);
        seekerDashboardActivity.filter_icon.setVisibility(View.INVISIBLE);

        userSeekerDTO = prefrence.getUserDTO(AppConstans.SEEKERDTO);

        etOldPassword = (CustomEdittext) view.findViewById(R.id.etOldPassword);
        etNewPassword = (CustomEdittext) view.findViewById(R.id.etNewPassword);
        etConfirmNewPassword = (CustomEdittext) view.findViewById(R.id.etConfirmNewPassword);
        UpdateBtn = (CustomButton) view.findViewById(R.id.UpdateBtn);
        UpdateBtn.setOnClickListener(this);
        changeLanguage(view);
        adMobView = view.findViewById(R.id.adMobView);
        // mAdView = (AdView) view.findViewById(R.id.adView);
        ProjectUtils.showAdd(getActivity(), mAdView, "ca-app-pub-3940256099942544/6300978111", adMobView);
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.UpdateBtn:
                Submit();
                break;
        }
    }


    private void Submit() {
        if (!passwordValidation()) {
            return;
        } else if (!checkpass()) {
            return;
        } else {
            if (NetworkManager.isConnectToInternet(getActivity())) {
                updatePassword();
            } else {
                ProjectUtils.showToast(getActivity(), getResources().getString(R.string.internet_connection));
            }

        }
    }


    public boolean passwordValidation() {
        if (!ProjectUtils.IsPasswordValidation(etOldPassword.getText().toString().trim())) {
            etOldPassword.setError(getResources().getString(R.string.val_pass_c));
            etOldPassword.requestFocus();
            return false;
        } else if (!ProjectUtils.IsPasswordValidation(etNewPassword.getText().toString().trim())) {
            etNewPassword.setError(getResources().getString(R.string.val_pass_c));
            etNewPassword.requestFocus();
            return false;
        } else
            return true;

    }

    private boolean checkpass() {
        if (etNewPassword.getText().toString().trim().equals("")) {
            etNewPassword.setError(errorMSG);
            return false;
        } else if (etConfirmNewPassword.getText().toString().trim().equals("")) {
            etConfirmNewPassword.setError(errorMSG);
            return false;
        } else if (!etNewPassword.getText().toString().trim().equals(etConfirmNewPassword.getText().toString().trim())) {
            etConfirmNewPassword.setError(errorMSG);
            return false;
        }
        return true;
    }


    public void updatePassword() {
        ProjectUtils.showProgressDialog(getActivity(), false, "Please wait...");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.PASSWORD_CHANGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ProjectUtils.pauseProgressDialog();
                        CommonDTO commonDTO = new CommonDTO();
                        commonDTO = new Gson().fromJson(response, CommonDTO.class);

                        if (commonDTO.getStaus().equalsIgnoreCase("true")) {
                            ProjectUtils.showToast(getActivity(), commonDTO.getMessage());
                        }else{
                            ProjectUtils.showToast(getActivity(), commonDTO.getMessage());

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProjectUtils.pauseProgressDialog();
                        Log.e("error_on_changepass_api", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstans.TYPE, "seeker");
                params.put(AppConstans.EMAIL, userSeekerDTO.getData().getEmail());
                params.put(AppConstans.OLD_PS, ProjectUtils.getEditTextValue(etOldPassword));
                params.put(AppConstans.PASSWORD, ProjectUtils.getEditTextValue(etNewPassword));
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void changeLanguage(View view) {

        CustomTextview lgOldPass, lgNewPass, lgConfirmP;

        lgConfirmP = (CustomTextview) view.findViewById(R.id.lgConfirmP);
        lgOldPass = (CustomTextview) view.findViewById(R.id.lgOldPass);
        lgNewPass = (CustomTextview) view.findViewById(R.id.lgNewPass);


        lgConfirmP.setText(data.getValidation_cps_empty());
        lgOldPass.setText(data.getOld_ps_enter());
        lgNewPass.setText(data.getPassword_login());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        seekerDashboardActivity = (SeekerDashboardActivity) activity;

    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
