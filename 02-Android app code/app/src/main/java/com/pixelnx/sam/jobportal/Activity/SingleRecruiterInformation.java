package com.pixelnx.sam.jobportal.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.DTO.SingleJobDTO;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterSingleDetails;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomEdittext;
import com.pixelnx.sam.jobportal.utils.CustomTextSubHeader;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SingleRecruiterInformation extends AppCompatActivity {
    private Context mContext;
    private CircleImageView ivLogoOrg;
    private CustomTextviewBold tvOrgLoc, tvOrgAddress, tvOrgWebsite, tvOrgDescription, tvName, tvType;
    private CustomButton btnUpdate;
    private CustomTextviewBold planDetails;
    private CustomTextSubHeader tvUsername;
    private String recruiter_id = "";
    private ModelRecruiterSingleDetails modelRecruiterSingleDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_recruiter_information);
        mContext = SingleRecruiterInformation.this;
        if (getIntent().hasExtra(AppConstans.R_ID))
            recruiter_id = getIntent().getStringExtra(AppConstans.R_ID);
        init();
    }

    private void init() {
        tvUsername = (CustomTextSubHeader) findViewById(R.id.tvUsername);
        ivLogoOrg = (CircleImageView) findViewById(R.id.ivLogoOrg);
        tvOrgLoc = (CustomTextviewBold) findViewById(R.id.tvOrgLoc);
        tvOrgAddress = (CustomTextviewBold) findViewById(R.id.tvOrgAddress);
        tvOrgWebsite = (CustomTextviewBold) findViewById(R.id.tvOrgWebsite);
        tvOrgDescription = (CustomTextviewBold) findViewById(R.id.tvOrgDescription);
        planDetails = (CustomTextviewBold) findViewById(R.id.planDetails);
        tvName = (CustomTextviewBold) findViewById(R.id.tvName);
        getRecruiterDetails();
    }

    private void getRecruiterDetails() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.RECRUITER_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("Single Job View", response.toString());
                            modelRecruiterSingleDetails = new Gson().fromJson(response, ModelRecruiterSingleDetails.class);
                            if (modelRecruiterSingleDetails.getStaus().equalsIgnoreCase("true")) {
                                showDetails(modelRecruiterSingleDetails.getData());
                            } else {
                                ProjectUtils.showToast(mContext, modelRecruiterSingleDetails.getMessage());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("recruiter_email", "e@gmail.com");
                // params.put(Consts.JOB_ID, jobid);
                Log.e("value", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);

    }

    private void showDetails(ModelRecruiterSingleDetails.Data data) {

        tvUsername.setText(chekNullValue(data.getName()));
        tvOrgLoc.setText(chekNullValue(data.getLocation()));
        tvOrgAddress.setText(chekNullValue(data.getAddress()));
        tvOrgWebsite.setText(chekNullValue(data.getWebsite()));
        tvOrgDescription.setText(chekNullValue(data.getDes()));
        tvType.setText(chekNullValue(data.getOrg_type()));
    }

    private String chekNullValue(String t) throws NullPointerException {

        if (t != null)
            return t;
        else
            return "";

    }


}
