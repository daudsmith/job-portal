package com.pixelnx.sam.jobportal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.pixelnx.sam.jobportal.Activity.SingleJob;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterAppliedSeeker;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;

import java.util.ArrayList;

public class AdapterAppliedJobs extends RecyclerView.Adapter<AdapterAppliedJobs.HolderAppliedJobs >  {


   private Context mContext ;
    private ArrayList<ModelRecruiterAppliedSeeker.Data> dataArrayList;
    int lastPosition= -1;

    public AdapterAppliedJobs(Context mContext, ArrayList<ModelRecruiterAppliedSeeker.Data> dataArrayList) {
        this.mContext = mContext;
        this.dataArrayList = dataArrayList;
    }

    @Override
    public HolderAppliedJobs onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rui_b,parent,false);

        AdapterAppliedJobs.HolderAppliedJobs holderAppliedJobs =new HolderAppliedJobs(view);

        return holderAppliedJobs;
    }

    @Override
    public void onBindViewHolder(HolderAppliedJobs holder, final int position) {
     holder.tvName.setText(dataArrayList.get(position).getName());
     holder.tvRole.setText(dataArrayList.get(position).getEmail());
     holder.tvMobile.setText(dataArrayList.get(position).getMno());

     holder.cardClick.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Intent intent = new Intent(mContext, SingleJob.class);
             intent.putExtra(Consts.SEEKER_ID, dataArrayList.get(position).getEmail());
             intent.putExtra(Consts.JOB_ID, "");
             mContext.startActivity(intent);
         }
     });
        setAnimation(holder.cardClick,position);
    }
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_from_bottom);
            animation.setDuration(700);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class HolderAppliedJobs  extends RecyclerView.ViewHolder{
        private CustomTextviewBold tvName;
        private CustomTextview tvRole, tvMobile, tvLocation, tvSkills,tvApplied;
        private CustomButton ViewBtn;
        private LinearLayout llApplied;
        private CardView cardClick;
        public HolderAppliedJobs(View itemView) {
            super(itemView);
            tvName = (CustomTextviewBold) itemView.findViewById(R.id.tvName);
            tvRole = (CustomTextview) itemView.findViewById(R.id.tvRole);
            tvMobile = (CustomTextview) itemView.findViewById(R.id.tvMobile);
            tvLocation = (CustomTextview) itemView.findViewById(R.id.tvLocation);
            tvSkills = (CustomTextview) itemView.findViewById(R.id.tvSkills);
            ViewBtn = (CustomButton) itemView.findViewById(R.id.ViewBtn);
            tvApplied = (CustomTextview) itemView.findViewById(R.id.tvApplied);
            cardClick = (CardView) itemView.findViewById(R.id.cardClick);
        }
    }

}
