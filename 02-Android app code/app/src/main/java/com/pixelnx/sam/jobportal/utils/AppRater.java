package com.pixelnx.sam.jobportal.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pixelnx.sam.jobportal.R;

import static java.security.AccessController.getContext;

public class AppRater {

    private final static String APP_TITLE = "Job Portal";// App Name
    private final static String APP_PNAME = "com.pixelnx.sam.jobportal";// Package Name

    private final static int DAYS_UNTIL_PROMPT = 0;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

    public static void app_launched(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) { return ; }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }



        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
          /*  if (System.currentTimeMillis() >= date_firstLaunch +
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {

            }*/
            showRateDialog(mContext, editor);
        }

        editor.commit();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle("Rate " + APP_TITLE);

        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);

        CustomTextSubHeader tv = new CustomTextSubHeader(mContext);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv.setText("If you enjoy using " + APP_TITLE + ", please take a moment to rate it.\n Thanks for your support!");

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) mContext). getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;


        tv.setWidth(width);

        tv.setPadding(4, 16, 4, 10);
        ll.addView(tv);

        CustomButton b1 = new CustomButton(mContext);
        b1.setText("Rate " + APP_TITLE);
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                dialog.dismiss();
            }
        });
        ll.addView(b1);

        CustomButton b2 = new CustomButton(mContext);
        b2.setText("Remind me later");
        b2.setBackground(mContext.getDrawable(R.drawable.tv_bg));
        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        b2.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        b2.setHeight(20);
        ll.addView(b2);

        Button b3 = new Button(mContext);
        b3.setText("No, thanks");
        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
        b3.setPadding(4, 16, 4, 16);
        ll.addView(b3);
        dialog.setContentView(ll);
        dialog.show();
    }
}
