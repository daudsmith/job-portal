package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;

public class ModelRecruiterInfo implements Serializable{

    String message = "";
    String staus = "";
    Data data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {

        String id = "";
        String name = "";
        String email = "";
        String mno = "";
        String ps = "";
        String gender = "";
        String current_address = "";
        String p_locaion = "";
        String job_type = "";
        String qua = "";
        String p_year = "";
        String cgpa = "";
        String aofs = "";
        String exp = "";
        String resume = "";
        String veri = "";
        String img = "";
        String counter = "";
        String status = "";
        String    org_type="";
        String is_updated= "";
        String website="";
        String des="";
        String address="";
        String plan ="";
        String pay  ="";
        String      pay_date="";
        String type="";
        String month="";
        String location="";

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getPlan() {
            return plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getPay() {
            return pay;
        }

        public void setPay(String pay) {
            this.pay = pay;
        }

        public String getPay_date() {
            return pay_date;
        }

        public void setPay_date(String pay_date) {
            this.pay_date = pay_date;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOrg_type() {
            return org_type;
        }

        public void setOrg_type(String org_type) {
            this.org_type = org_type;
        }

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getIs_updated() {
            return is_updated;
        }

        public void setIs_updated(String is_updated) {
            this.is_updated = is_updated;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMno() {
            return mno;
        }

        public void setMno(String mno) {
            this.mno = mno;
        }

        public String getPs() {
            return ps;
        }

        public void setPs(String ps) {
            this.ps = ps;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCurrent_address() {
            return current_address;
        }

        public void setCurrent_address(String current_address) {
            this.current_address = current_address;
        }

        public String getP_locaion() {
            return p_locaion;
        }

        public void setP_locaion(String p_locaion) {
            this.p_locaion = p_locaion;
        }

        public String getJob_type() {
            return job_type;
        }

        public void setJob_type(String job_type) {
            this.job_type = job_type;
        }

        public String getQua() {
            return qua;
        }

        public void setQua(String qua) {
            this.qua = qua;
        }

        public String getP_year() {
            return p_year;
        }

        public void setP_year(String p_year) {
            this.p_year = p_year;
        }

        public String getCgpa() {
            return cgpa;
        }

        public void setCgpa(String cgpa) {
            this.cgpa = cgpa;
        }

        public String getAofs() {
            return aofs;
        }

        public void setAofs(String aofs) {
            this.aofs = aofs;
        }

        public String getExp() {
            return exp;
        }

        public void setExp(String exp) {
            this.exp = exp;
        }

        public String getResume() {
            return resume;
        }

        public void setResume(String resume) {
            this.resume = resume;
        }

        public String getVeri() {
            return veri;
        }

        public void setVeri(String veri) {
            this.veri = veri;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getCounter() {
            return counter;
        }

        public void setCounter(String counter) {
            this.counter = counter;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
