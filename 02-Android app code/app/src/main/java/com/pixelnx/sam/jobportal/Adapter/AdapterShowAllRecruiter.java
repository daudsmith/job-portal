package com.pixelnx.sam.jobportal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pixelnx.sam.jobportal.Activity.SingleRecruiterInformation;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterPostJobs;
import com.pixelnx.sam.jobportal.DTOCI.ShowAllRecruiter;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterShowAllRecruiter extends RecyclerView.Adapter<AdapterShowAllRecruiter.HolderAdapterShowAllRecruiter> {
    private Context mContext;
    private ArrayList<ShowAllRecruiter.Data> recruiterList;
    private String base_url = "";
    private String searchString="";
    private ArrayList<ShowAllRecruiter.Data> tempRecruiterList;



    public AdapterShowAllRecruiter(ArrayList<ShowAllRecruiter.Data> recruiterList, Context mContext, String base_url) {
        this.mContext = mContext;
        this.recruiterList = recruiterList;
        base_url = base_url;
        tempRecruiterList=new ArrayList<>();
        this.tempRecruiterList.addAll(recruiterList);
    }

    @Override
    public HolderAdapterShowAllRecruiter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_single_recruiter, parent, false);
        HolderAdapterShowAllRecruiter holderAdapterShowAllRecruiter = new HolderAdapterShowAllRecruiter(view);
        return holderAdapterShowAllRecruiter;
    }

    @Override
    public void onBindViewHolder(HolderAdapterShowAllRecruiter holder, int position) {
      final  ShowAllRecruiter.Data data = recruiterList.get(position);

        holder.tvMobile.setText("" + data.getMno());
        holder.tvName.setText("" + data.getName());
        holder.tvRole.setText("" + data.getWebsite());
        Glide.with(mContext).load(base_url + data.getImg()).placeholder(R.drawable.user_drawer).into(holder.ivRecruiterImage);
       Float curRate = Float.valueOf(data.getRat_rating());
        holder.ratingbar.setRating(curRate);

        String country = recruiterList.get(position).getName().toLowerCase(Locale.getDefault());

        if (country.contains(searchString)) {
            int startPos = country.indexOf(searchString);
            int endPos = startPos + searchString.length();

            Spannable spanText = Spannable.Factory.getInstance().newSpannable(holder.tvName.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
            spanText.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorAccent)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.tvName.setText(spanText, TextView.BufferType.SPANNABLE);
        }


        holder.cardClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return recruiterList.size();
    }


    public void filter(String charText) {

        this.searchString = charText;
        charText = charText.toLowerCase(Locale.getDefault());
        recruiterList.clear();
        if (charText.length() == 0) {
            recruiterList.addAll(tempRecruiterList);
        } else {
            for (ShowAllRecruiter.Data data : tempRecruiterList) {
                if (data.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    recruiterList.add(data);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class HolderAdapterShowAllRecruiter extends RecyclerView.ViewHolder {
        public CircleImageView ivRecruiterImage;
        public CustomTextviewBold tvName;
        public CustomTextview tvRole, tvMobile;
        public LinearLayout llNext;
        public RatingBar ratingbar;
        public CardView cardClick;

        public HolderAdapterShowAllRecruiter(View itemView) {
            super(itemView);
            ivRecruiterImage = (CircleImageView) itemView.findViewById(R.id.ivRecruiterImage);
            tvName = (CustomTextviewBold) itemView.findViewById(R.id.tvName);
            tvRole = (CustomTextview) itemView.findViewById(R.id.tvRole);
            tvMobile = (CustomTextview) itemView.findViewById(R.id.tvMobile);
            llNext = (LinearLayout) itemView.findViewById(R.id.llNext);
            ratingbar = (RatingBar) itemView.findViewById(R.id.ratingbar);
            cardClick = (CardView) itemView.findViewById(R.id.cardClick);
        }
    }
}



