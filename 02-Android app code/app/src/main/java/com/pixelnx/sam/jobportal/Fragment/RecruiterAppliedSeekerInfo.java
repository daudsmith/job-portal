package com.pixelnx.sam.jobportal.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Activity.RecruiterDashboardActivity;
import com.pixelnx.sam.jobportal.Adapter.AdapterAppliedJobs;
import com.pixelnx.sam.jobportal.Adapter.JobApplication_adapter;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterAppliedSeeker;
import com.pixelnx.sam.jobportal.DTOCI.ModelRecruiterPostJobs;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecruiterAppliedSeekerInfo extends Fragment {
    View view   ;
    private RecyclerView rvJobAppliction;
    private ImageView fab_image;
    RecruiterDashboardActivity recruiterDashboardActivity;
    private SharedPrefrence sharedPrefrence;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<ModelRecruiterAppliedSeeker.Data> dataArrayList;
     private AdapterAppliedJobs adapterAppliedJobs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    view=  inflater.inflate(R.layout.fragment_job_application, container, false);
        sharedPrefrence = SharedPrefrence.getInstance(getActivity());
        recruiterDashboardActivity.tvJobs.setVisibility(View.VISIBLE);
        recruiterDashboardActivity.tvJobs.setText(getResources().getString(R.string.hed_posted_job));
        recruiterDashboardActivity.search_icon.setVisibility(View.INVISIBLE);
        recruiterDashboardActivity.filter_icon.setVisibility(View.INVISIBLE);
        init(view);
        return view;
    }

    private void init(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
           rvJobAppliction = (RecyclerView) view.findViewById(R.id.rvJobAppliction);
        fab_image = (ImageView) view.findViewById(R.id.fab_image);


        fab_image.setVisibility(View.GONE);
           showAppiedSeekers();
    }

    private void showAppiedSeekers() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.APPLIED_SEEKER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            Log.e("REQUITER JOBS",  response.toString());
                            ModelRecruiterAppliedSeeker appliedSeeker = new Gson().fromJson(response, ModelRecruiterAppliedSeeker.class);
                            if (appliedSeeker.getStaus().equalsIgnoreCase("true")) {
                                dataArrayList = appliedSeeker.getData();
                                LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                linearLayoutManagerVertical.supportsPredictiveItemAnimations();
                                rvJobAppliction.setLayoutManager(linearLayoutManagerVertical);
                                if (dataArrayList.size() != 0) {
                                    rvJobAppliction.setVisibility(View.VISIBLE);
                                 //   llIcon.setVisibility(View.GONE);
                                    adapterAppliedJobs = new AdapterAppliedJobs( getActivity(),dataArrayList);
                                    rvJobAppliction.setAdapter( adapterAppliedJobs );
                                } else {
                                    rvJobAppliction.setVisibility(View.GONE);
                                 //   llIcon.setVisibility(View.VISIBLE);
                                }
                            } else {
                                rvJobAppliction.setVisibility(View.GONE);
                               // llIcon.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstans.RE_EMAIL, sharedPrefrence.getUserDTO(AppConstans.SEEKERDTO).getData().getEmail());
                Log.e("value", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    recruiterDashboardActivity=(RecruiterDashboardActivity)activity;
    }
}
