package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;

public class JobPostDTO implements Serializable {
    String staus="";
    String message="";

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
