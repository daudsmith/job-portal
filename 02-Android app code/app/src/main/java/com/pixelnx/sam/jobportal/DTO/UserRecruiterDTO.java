
package com.pixelnx.sam.jobportal.DTO;

import java.io.Serializable;

public class UserRecruiterDTO extends BaseDTO implements Serializable {

   // private int code;
    //private boolean status;
   private String staus="";
    private String message = "";
    public Data data;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

    }


}

