package com.pixelnx.sam.jobportal.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.pixelnx.sam.jobportal.Activity.FullDetailsActivity;
import com.pixelnx.sam.jobportal.DTO.ActiveJobDTO;
import com.pixelnx.sam.jobportal.DTOCI.AllJobs;
import com.pixelnx.sam.jobportal.DTOCI.ModelLanguageDTO;
import com.pixelnx.sam.jobportal.Fragment.SeekerHomeFragment;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.CustomButton;
import com.pixelnx.sam.jobportal.utils.CustomTextview;
import com.pixelnx.sam.jobportal.utils.CustomTextviewBold;

import java.util.ArrayList;

/**
 * Created by shubham on 10/8/17.
 */

public class AdapterDashboard extends RecyclerView.Adapter<AdapterDashboard.Mainholder> {


    private ArrayList<AllJobs.Data> dataArrayList;
    private Context mContext;
    private int lastPosition = -1;
private SharedPrefrence  prefrence;
ModelLanguageDTO.Data modelLanguageDTO;
private  String btString="";
    public AdapterDashboard(ArrayList<AllJobs.Data> dataArrayList, Context mContext) {
        this.dataArrayList = dataArrayList;
        this.mContext = mContext;
        prefrence=SharedPrefrence.getInstance(mContext);

    }

    @Override
    public Mainholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dashboard, parent, false);
        Mainholder mainHolder = new Mainholder(view);

        return mainHolder;
    }

    @Override
    public void onBindViewHolder(final Mainholder holder, final int position) {
        try {
            btString = prefrence.getLanguage(AppConstans.LANGUAGE).getData().getView_btn();
        }catch (Exception r){r.printStackTrace();}
        holder.tvdeisgnation.setText(dataArrayList.get(position).getDesignation());
        holder.tvOrganisation.setText(dataArrayList.get(position).getAuthor());
        holder.tvExperience.setText(dataArrayList.get(position).getExp());
        holder.tvLocation.setText(dataArrayList.get(position).getJob_location());
        holder.ViewBtn.setText(btString);
        if (dataArrayList.get(position).getTechnology().equals("")) {
            holder.tvSkills.setText("N/A");

        } else {
            holder.tvSkills.setText(dataArrayList.get(position).getTechnology());
        }
        holder.ViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FullDetailsActivity.class);
                intent.putExtra(AppConstans.ACTIVE_JOBS_DTO, dataArrayList.get(position).getId());
                intent.putExtra(AppConstans.IS_APPLIED, dataArrayList.get(position).isApplied());
                mContext.startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        if (dataArrayList.get(position).isApplied()) {
            holder.tvApplied.setVisibility(View.VISIBLE);
            holder.tvApplied.setTextSize(12);
        }
        else
            holder.tvApplied.setVisibility(View.GONE);
if (position>3)
        setAnimation(holder.rlDashboard,position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_from_bottom);
            animation.setDuration(700);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class Mainholder extends RecyclerView.ViewHolder {
        private CustomTextviewBold tvdeisgnation,tvApplied;
        private CustomTextview tvOrganisation, tvExperience, tvLocation, tvSkills;
        private CustomButton ViewBtn;
        private LinearLayout container;
        private RelativeLayout rlDashboard;


        public Mainholder(View itemView) {
            super(itemView);
            tvdeisgnation = (CustomTextviewBold) itemView.findViewById(R.id.tvdeisgnation);
            tvOrganisation = (CustomTextview) itemView.findViewById(R.id.tvOrganisation);
            tvExperience = (CustomTextview) itemView.findViewById(R.id.tvExperience);
            tvLocation = (CustomTextview) itemView.findViewById(R.id.tvLocation);
            tvSkills = (CustomTextview) itemView.findViewById(R.id.tvSkills);
            ViewBtn = (CustomButton) itemView.findViewById(R.id.ViewBtn);
            tvApplied = (CustomTextviewBold) itemView.findViewById(R.id.tvApplied);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            rlDashboard = (RelativeLayout) itemView.findViewById(R.id.rlDashboard);
        }
    }
}