package com.pixelnx.sam.jobportal.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.google.android.gms.ads.AdView;
import com.pixelnx.sam.jobportal.DTO.SingleJobDTO;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

public class PdfView extends AppCompatActivity {
    WebView mWebView;
    private SingleJobDTO singleJobDTO;
    private AdView mAdView;
    private View adMobView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);
    //    mAdView=(AdView) findViewById(R.id.adView);
        adMobView = findViewById(R.id.adMobView);

        if (getIntent().hasExtra(Consts.SINGLE_JOB_DTO)) {
            singleJobDTO = (SingleJobDTO) getIntent().getSerializableExtra(Consts.SINGLE_JOB_DTO);
        }
        mWebView = new WebView(PdfView.this);
        mWebView.getSettings().setJavaScriptEnabled(true);

      //  mWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + singleJobDTO.getData().getProfile().getResume());
        setContentView(mWebView);
        ProjectUtils.showAdd(PdfView.this, mAdView, "ca-app-pub-3940256099942544/6300978111", adMobView);


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
