package com.pixelnx.sam.jobportal.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixelnx.sam.jobportal.Activity.SeekerDashboardActivity;
import com.pixelnx.sam.jobportal.Adapter.AdapterAppliedJobs;
import com.pixelnx.sam.jobportal.Adapter.AdapterDashboard;
import com.pixelnx.sam.jobportal.DTOCI.AllJobs;
import com.pixelnx.sam.jobportal.DTOCI.ModelAppliedJobs;
import com.pixelnx.sam.jobportal.R;
import com.pixelnx.sam.jobportal.network.NetworkManager;
import com.pixelnx.sam.jobportal.preferences.SharedPrefrence;
import com.pixelnx.sam.jobportal.utils.AppConstans;
import com.pixelnx.sam.jobportal.utils.Consts;
import com.pixelnx.sam.jobportal.utils.ProjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SeekerAppliedJobs extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private LinearLayout llIcon;
    private SwipeRefreshLayout swipe_refresh_layout;
    private RecyclerView recyclerview = null;
    private AdapterDashboard adapterDashboard;
    private ArrayList<AllJobs.Data> dataArrayList;
    private LinearLayoutManager linearLayoutManagerVertical;
    private SharedPrefrence prefrence;
    private SeekerDashboardActivity seekerDashboardActivity;
    View view;
    private ImageView img;
     private AnimationDrawable anim;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_seeker_my_applied_jobs, container, false);
            init(view);

    return view;

    }

    private void init(View view) {

        llIcon=(LinearLayout )view.findViewById(R.id.llIcon);
        swipe_refresh_layout=(SwipeRefreshLayout )view.findViewById(R.id.swipe_refresh_layout);
        prefrence=SharedPrefrence.getInstance(getActivity());
        recyclerview=(RecyclerView )view.findViewById(R.id.rv_appliedJobs);
        img = (ImageView) view.findViewById(R.id.img);
        seekerDashboardActivity.tvJobs.setVisibility(View.VISIBLE);
        seekerDashboardActivity.tvJobs.setText(prefrence.getLanguage(AppConstans.LANGUAGE).getData().getMy_a_applied());
        seekerDashboardActivity.search_icon.setVisibility(View.INVISIBLE);
        seekerDashboardActivity.filter_icon.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onResume() {
        super.onResume();

        swipe_refresh_layout.setOnRefreshListener(this);
        swipe_refresh_layout.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        Log.e("Runnable", "FIRST");
                                        if (NetworkManager.isConnectToInternet(getActivity())) {
                                            swipe_refresh_layout.setRefreshing(false);
                                            showAllAppliedJobs();

                                        } else {
                                            ProjectUtils.showToast(getActivity(), getResources().getString(R.string.internet_connection));
                                        }
                                    }
                                }
        );
    }

    @Override
    public void onRefresh() {
        showAllAppliedJobs();
    }
    private void showAllAppliedJobs() {
        Animation();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstans.BASE_URL + AppConstans.MY_APPLIED_JOB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ProjectUtils.pauseProgressDialog();
stopAnimation();
                        try {
                            AllJobs allJobs= new AllJobs();
                            allJobs = new Gson().fromJson(response, AllJobs.class);

                            if (allJobs.getStaus().equals("true")) {
                                swipe_refresh_layout.setRefreshing(false);
                                dataArrayList=new ArrayList<>();
                                dataArrayList = allJobs.getData();
                                linearLayoutManagerVertical     = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                recyclerview.setLayoutManager(linearLayoutManagerVertical);

                                if (dataArrayList.size() != 0) {
                                    recyclerview.setVisibility(View.VISIBLE);
                                    llIcon.setVisibility(View.GONE);
                                    for (int i=0;i<dataArrayList.size();i++){
                                        dataArrayList.get(i).setApplied(true);
                                    }
                                    adapterDashboard = new AdapterDashboard(dataArrayList, getActivity());
                                    recyclerview.setAdapter(adapterDashboard);
                                }
                                stopAnimation();
                            } else {
                                swipe_refresh_layout.setRefreshing(false);
                                ProjectUtils.showToast(getActivity(), allJobs.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            swipe_refresh_layout.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProjectUtils.pauseProgressDialog();
                       // swipeRefreshLayout.setRefreshing(false);
                        stopAnimation();
                        Log.d("ALL JOB RESPONSE", "Login Error:" + error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstans.EMAIL, prefrence.getUserDTO(AppConstans.SEEKERDTO).getData().getEmail());
             //   params.put(Consts.JOB_ID, jobApplicationDTOList.get(position).getId());

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
    private void Animation() {
        img.setVisibility(View.VISIBLE);
        anim = (AnimationDrawable) img.getDrawable();
        img.post(run);
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {
            anim.start();
        }
    };

    private void stopAnimation(){
        anim.stop();
        img.setVisibility(View.GONE);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        seekerDashboardActivity = (SeekerDashboardActivity) activity;
    }
}
