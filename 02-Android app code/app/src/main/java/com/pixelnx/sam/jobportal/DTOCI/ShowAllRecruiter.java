package com.pixelnx.sam.jobportal.DTOCI;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowAllRecruiter implements Serializable {
    public String staus = "";
  public    String message = "";
    public ArrayList<Data> data;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data {
        public String id="";
        public String name="";
        public String email="";
        public String mno="";
        public String org_type="";
        public String location="";
        public String address="";
        public String website="";
        public String des="";
        public String veri="";
        public String img="";
        public String plan="";
        public String pay  ="";
        public String pay_date="";
        public String type ="";
        public String month="";
        public String show_on_reg="";
        public String status="";
        public String pay_count="";
        public String paypal="";
        public String counter="";
        public String google_id="";
        public String rat_rating="";
        public String comment="";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMno() {
            return mno;
        }

        public void setMno(String mno) {
            this.mno = mno;
        }

        public String getOrg_type() {
            return org_type;
        }

        public void setOrg_type(String org_type) {
            this.org_type = org_type;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }

        public String getVeri() {
            return veri;
        }

        public void setVeri(String veri) {
            this.veri = veri;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getPlan() {
            return plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getPay() {
            return pay;
        }

        public void setPay(String pay) {
            this.pay = pay;
        }

        public String getPay_date() {
            return pay_date;
        }

        public void setPay_date(String pay_date) {
            this.pay_date = pay_date;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getShow_on_reg() {
            return show_on_reg;
        }

        public void setShow_on_reg(String show_on_reg) {
            this.show_on_reg = show_on_reg;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPay_count() {
            return pay_count;
        }

        public void setPay_count(String pay_count) {
            this.pay_count = pay_count;
        }

        public String getPaypal() {
            return paypal;
        }

        public void setPaypal(String paypal) {
            this.paypal = paypal;
        }

        public String getCounter() {
            return counter;
        }

        public void setCounter(String counter) {
            this.counter = counter;
        }

        public String getGoogle_id() {
            return google_id;
        }

        public void setGoogle_id(String google_id) {
            this.google_id = google_id;
        }

        public String getRat_rating() {
            return rat_rating;
        }

        public void setRat_rating(String rat_rating) {
            this.rat_rating = rat_rating;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }


}
